import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
from pathlib import Path
import os
from bin_heap_dict import BinHeapDict
from itertools import combinations
from collections import defaultdict

ROUND_PRECISION = 3
def truncate(num, n):
    integer = int(num * (10**n))/(10**n)
    return float(integer)


def urf(f:float):
    """
    unique round float
    """
    return round(f,ROUND_PRECISION)
    # return int(f * (10**R))
def urft(t):
    return urf(t[0]),urf(t[1])

def generate_weighted_tree(num_nodes, seed=None):
    tree = nx.generators.gn_graph(num_nodes, seed=seed).reverse()
    tree = nx.generators.stochastic_graph(tree, weight='length')
    return tree


def generate_weighted_graph(num_nodes, connect_prob=0.4, seed=None, remove_dir_prob=0.5, layout=nx.random_layout):
    g: nx.DiGraph = nx.fast_gnp_random_graph(
        num_nodes, connect_prob, seed=seed).to_directed()
    pos = layout(g)
    g.graph["pos"] = pos
    if remove_dir_prob is not None:
        edges = list(g.edges())
        random_edges_ids = np.arange(len(g.edges()))
        np.random.shuffle(random_edges_ids)
        random_edges = [edges[i] for i in random_edges_ids[0:len(edges)//2]]
        g.remove_edges_from(random_edges)
    for e in g.edges():
        length = sum((pos[e[0]] - pos[e[1]])**2)**(1/2)
        g.edges[e]["length"] = length

    for n in g.nodes():
        g.nodes[n]['pos'] = [float(c) for c in pos[n]]
    return g


def generate_weighted_graph_complex(num_nodes, connect_prob=0.4, seed=None, remove_dir_prob=0.5, num_g=1):
    return nx.disjoint_union_all([generate_weighted_graph(num_nodes, connect_prob=connect_prob, seed=seed, remove_dir_prob=remove_dir_prob) for i in range(num_g)])


def generate_test_resources(num_tests, num_nodes, output_folder):
    Path(f"./{output_folder}").mkdir(parents=True, exist_ok=True)
    for i in tqdm(range(num_tests)):
        g = generate_weighted_graph_complex(num_nodes, seed=i, num_g=1)
        del g.graph["pos"]
        util.dump_graph(g, os.path.join(output_folder, f"graph{num_nodes}-{i}"))

def generate_basic_test(output_folder,num_nodes):
    g = nx.DiGraph()
    g.add_edge(0,2,length=1)
    g.add_edge(1,2,length=1)
    g.add_edge(2,100,length=100)
    g.add_edge(100,3,length=100)
    for i in range(2,num_nodes):
        g.add_edge(i,i+1,length=1)
    g.nodes[0]["pos"] = (0,1)
    g.nodes[1]["pos"] = (0,-1)
    g.nodes[2]["pos"] = (1,0)
    g.nodes[100]["pos"] = (2,1)
    # g.nodes[3]["pos"] = (3,0)
    for i in range(3,num_nodes+1):
        g.nodes[i]["pos"] = (i,0)
    print(g.nodes[4])
    nx.draw_networkx(g, pos=util.get_pos(g))
    plt.show()
    util.dump_graph(g, os.path.join(output_folder, f"graph_{g.number_of_nodes()}.json"))
    
def generate_grid_graph(output_folder, num_nodes_x, num_nodes_y):
    np.random.seed(42)
    g = nx.DiGraph()
    grid_x, grid_y = np.mgrid[0:num_nodes_x:1, 0:num_nodes_y:1]
    poses = np.vstack((np.ravel(grid_x), np.ravel(grid_y))).T
    for i,pos in enumerate(poses):
        g.add_node(i,pos=[float(p) for p in pos])
    for i in range(num_nodes_x):
        vertical_edges = [(i*num_nodes_y + k,i*num_nodes_y + (k+1)) for k in range(0, num_nodes_y-1)]
        vertical_edges.extend([(i*num_nodes_y + (k+1),i*num_nodes_y + k) for k in range(0, num_nodes_y-1)])
        g.add_edges_from(vertical_edges, length=0.5 + np.random.rand())
    for i in range(num_nodes_y):
        horizontal_edges = [(i+k*num_nodes_y,i+(k+1)*num_nodes_y) for k in range(0,num_nodes_x-1)]
        horizontal_edges.extend([(i+(k+1)*num_nodes_y,i+k*num_nodes_y) for k in range(0,num_nodes_x-1)])
        g.add_edges_from(horizontal_edges, length=0.5 + np.random.rand())
    g.remove_edges_from([(0,5),(5,0),(0,1),(1,0)])
    g.add_edge(0,5,length=100)
    g.add_edge(5,0,length=100)
    g.add_edge(0,1,length=90)
    g.add_edge(1,0,length=90)
    g.add_edge(0,6,length=0.001)
    util.dump_graph(g, os.path.join(output_folder, f"grid_{num_nodes_x}x{num_nodes_y}.json"))

def polar_to_cartesian(r, phi):
    return (r * np.cos(phi), r * np.sin(phi))
    
def build_hex_nodes_from_center(center, length):
    rel_polar_coords = [(length, np.pi / 3 * i) for i in range(6)]
    rel_cart_coords = [polar_to_cartesian(*rpc) for rpc in rel_polar_coords]
    abs_cart_coords = [(center[0] + rcc[0], center[1] + rcc[1]) for rcc in rel_cart_coords]
    # print(center)
    # print(rel_polar_coords)
    # print(rel_cart_coords)
    # print(abs_cart_coords)
    # print()
    return abs_cart_coords

def add_hex_nodes_to_graph(g:nx.DiGraph,coords2node:dict,hex_coords:list,inner_con_prob:float):
    cur_node_id = g.number_of_nodes()
    new_nodes_coords = [urft(hc) for hc in hex_coords if urft(hc) not in coords2node]
    new_nodes_names = range(cur_node_id, cur_node_id+len(new_nodes_coords))
    for nn,nc in zip(new_nodes_names, new_nodes_coords):
        g.add_node(nn, pos=nc)
        coords2node[nc] = nn
    for ind_pair in combinations(range(6),2):
        a,b = ind_pair
        
        coords_a, coords_b = urft(hex_coords[a]), urft(hex_coords[b])
        node_a, node_b = coords2node[coords_a], coords2node[coords_b]
        # print(ind_pair, node_a, node_b)
        is_neighbors = abs(ind_pair[0] - ind_pair[1]) == 1 or abs(ind_pair[0] - ind_pair[1]) == 5
        if is_neighbors:
            g.add_edge(node_a,node_b)
            g.add_edge(node_b,node_a)
        elif np.random.rand() < inner_con_prob:
            g.add_edge(node_a,node_b)
            g.add_edge(node_b,node_a)

def generate_hex_graph(num_nodes, length, inner_con_prob, shake_nodes):
    g = nx.DiGraph()
    coords2node = dict()
    
    sqrt3 = 3**(1/2) * length
    first_center = 0,0
    visited_centers = set()
    centers_to_visit = BinHeapDict()
    centers_to_visit.insert( (g.number_of_nodes(), first_center) )
    while g.number_of_nodes() < num_nodes:
        _,center = centers_to_visit.extract_min()
        visited_centers.add(center)
        hex_coords = build_hex_nodes_from_center(center,length)
        add_hex_nodes_to_graph(g,coords2node,hex_coords,inner_con_prob)
        # move only to the right-down hexas
        for phi in [3*np.pi/2,11*np.pi/6,np.pi/6]:
            rel_x,rel_y = polar_to_cartesian(sqrt3,phi)
            new_center = center[0] + rel_x, center[1] + rel_y
            if new_center in visited_centers:
                continue
            centers_to_visit.insert( (g.number_of_nodes(), new_center) )
    if shake_nodes:
        pass
    for e in g.edges():
        # print(e)
        length = ((np.array(g.nodes[e[0]]["pos"]) - np.array(g.nodes[e[1]]["pos"]))**2).sum()**(1/2)
        # print(length)
        g.edges[e]["length"] = length
    return g

def generate_hex_graphs(out_folder, num_graphs, num_nodes, length, inner_con_prob, shake_nodes):
    for i in tqdm(range(num_graphs)):
        g = generate_hex_graph(num_nodes, length, inner_con_prob, shake_nodes)
        util.dump_graph(g, f"{out_folder}/hex{num_nodes}-{i}.json")


# def get_cell_id_by_pos(pos,max_x,max_y,n_cells_x,n_cells_y):
#     ...

# def generate_connections_for_node(pos,k,cells_dict):
#     ...

def generate_node_pos(p1,p2):
    x1,y1=p1
    x2,y2=p2
    x = x1 + np.random.rand()*(x2-x1)
    y = y1 + np.random.rand()*(y2-y1)
    return x,y

def cell_coords(id_cell_x, id_cell_y, length):
    x1,y1 = id_cell_x * length, id_cell_y * length
    x2,y2 = x1 + length, y1 + length
    return (x1,y1),(x2,y2)

def l2_distance(p1,p2):
    return np.linalg.norm(np.array(p1)-np.array(p2))

def get_neighbor_cells_indices(id_cell_x,id_cell_y,n_cells_x,n_cells_y):
    x_inds = id_cell_x + np.arange(-1,2)
    y_inds = id_cell_y + np.arange(-1,2)
    inds = np.zeros((3,3,2))
    inds[:,:,0] = x_inds[:,None]
    inds[:,:,1] = y_inds
    result_inds = []
    for ind in inds.reshape(-1,2):
        # if (ind == [0,0]).all():
        #     continue
        if ind[0] < 0 or ind[0] >= n_cells_x:
            continue
        if ind[1] < 0 or ind[1] >= n_cells_y:
            continue
        result_inds.append(tuple(ind))
    return result_inds

def get_neighbor_nodes(node:int,cell_id:Tuple[int,int],k:int,g:nx.DiGraph,cells_dict:dict,n_cells_x:int,n_cells_y:int):
    node_pos = g.nodes[node]["pos"]
    cell_id_x,cell_id_y = cell_id
    cells_around = get_neighbor_cells_indices(cell_id_x,cell_id_y,n_cells_x,n_cells_y)
    if node == 4:
        print(node)
        print(cell_id,cells_around)
    # print([cells_dict[cell_id] for cell_id in cells_around])
    nodes_around = sum([cells_dict[cell_id] for cell_id in cells_around],start=[])
    nodes_around = [n for n in nodes_around if n != node]
    distances_around = [l2_distance(node_pos, g.nodes[n]["pos"]) for n in nodes_around]
    bin_heap = BinHeapDict((dst,n) for dst,n in zip(distances_around,nodes_around))
    neighbors, distances = [], []
    for i in range(min(k,len(nodes_around))):
        dst,n = bin_heap.extract_min()
        if len(g[n]) < k:
            neighbors.append(n)
            distances.append(dst)
    return neighbors, distances

def generate_knn_graph(n_nodes,k,n_cell_nodes,length=1,random_seed=42)->nx.DiGraph:
    np.random.seed(random_seed)
    # assert k > n_cell_nodes
    g = nx.DiGraph()
    n_cells = n_nodes // n_cell_nodes
    n_cells_x = n_cells_y = int(n_cells ** (1/2)) + 1
    # cells_dict maps cell_id to nodes poses
    cells_dict = defaultdict(list)
    cell_ids_x = np.arange(n_cells_x)
    cell_ids_y = np.arange(n_cells_y)
    # cell_ids[i,j] == (i,j), it is container for ids
    cell_ids = np.zeros((n_cells_x,n_cells_y,2))
    cell_ids[:,:,0] = cell_ids_x[:,None]
    cell_ids[:,:,1] = cell_ids_y
    # node_to_cell_id
    node_to_cell_id = dict()
    
    # generate nodes poses in every cell
    for cell_id in cell_ids.reshape(-1,2):
        lb_cell_coord,ru_cell_coord = cell_coords(cell_id[0],cell_id[1],length)
        cell_nodes_poses = [generate_node_pos(lb_cell_coord,ru_cell_coord) for i in range(n_cell_nodes)]
        cell_nodes_ids = range(g.number_of_nodes(), g.number_of_nodes() + n_cell_nodes)        
        cells_dict[tuple(cell_id)].extend(cell_nodes_ids)
        g.add_nodes_from(((n,{"pos":pos}) for n,pos in zip(cell_nodes_ids,cell_nodes_poses)))
        node_to_cell_id.update({node_id:cell_id for node_id in cell_nodes_ids})

    # find k neighbors per node and create edges
    for node in g.nodes():
        neighbors,dists = get_neighbor_nodes(node,node_to_cell_id[node],k,g,cells_dict,n_cells_x,n_cells_y)
        # print(node)
        # print('\t',neighbors)
        for n,dst in zip(neighbors,dists):
            if g.has_edge(node,n):
                continue
            g.add_edge(node,n,length=dst)
            g.add_edge(n,node,length=dst)
    return g

def generate_knn_graphs(out_folder,num_graphs,n_nodes,cell_length,con_prob,n_cell_nodes=4):
    for i in tqdm(range(num_graphs)):
        g = generate_knn_graph(n_nodes,con_prob,cell_length,n_cell_nodes)
        util.dump_graph(g)

if __name__ == "__main__":
    # generate_test_resources(500,10,"tests/resource/10")
    # generate_test_resources(100,100,"tests/resource/100")
    # generate_test_resources(10,1000,"tests/resource/1000")
    # generate_basic_test("tests/resource/basic", 20)
    # generate_grid_graph("tests/resource/basic", 4,5)
    # generate_hex_graphs("tests/resource/hex/10",1000,10, 1,0.5,False)
    for i in range(3,10):
        g = generate_knn_graph(n_nodes=10,k=5,n_cell_nodes=5,random_seed=i)
        nx.draw_networkx(g, pos=util.get_pos(g))
        plt.show()
    