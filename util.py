from operator import ne
import networkx as nx
from typing import Iterable, Optional, Tuple, List, Set, Dict
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
import numpy as np
import json
import io
import pytest

def log(msg, log_path="log.log"):
    with open(log_path, 'a') as fo:
        fo.write(str(msg) + "\n")


def get_neighbors(g: nx.DiGraph, nodes: Iterable[str], generator=True) -> Iterable[str]:
    if not isinstance(nodes,set):
        nodes = set(nodes)
    if generator:
        for node in nodes:
            for k in g[node].keys():
                if k not in nodes:
                    yield k
    # else:
    #     for node in nodes:
    #         res.update(g[node].keys())
    #     res = res - nodes_set
    #     return list(res)


# def get_neighbors_incoming_edges(g: nx.DiGraph, nodes: Iterable[str]):
#     res = defaultdict(set)
#     nodes_set = set(nodes)
#     for node in nodes:
#         for edge in g.edges(node):
#             if edge[1] not in nodes_set:
#                 res[edge[1]].add(edge)
#     return dict(res)


def get_subtree(tree: nx.DiGraph, node: str):
    to_visit = set([node])
    visited = set()
    subtree = nx.DiGraph()
    while len(to_visit) > 0:
        cur = to_visit.pop()
        # assert cur not in visited, f"Found cycle in tree: node '{cur}'"
        subtree.add_node(cur)
        edges = tree.edges.data(nbunch=[cur])
        subtree.add_edges_from(edges)  # (cur, data='length'))
        to_visit.update((n for n in get_neighbors(tree, [cur], generator=True)))# if n not in visited
        visited.add(cur)
    return subtree


def get_tree_info(tree: nx.DiGraph, source: str, source_dst=0, filter:set=None):
    # ensure is the set which every node must NOT belong to
    if filter is None:
        filter = set()

    distances = dict()
    parents = dict()
    if source in filter:
        return distances, parents
    distances[source] = source_dst
    to_visit = set([source])
    while len(to_visit) > 0:
        cur = to_visit.pop()
        # TODO use generator for neighbors
        neighbors = [neighbor for neighbor in get_neighbors(tree, [cur], generator=True) if neighbor not in filter]
        to_visit.update(neighbors)
        
        distances.update({n: distances[cur] + tree.edges[cur, n]['length'] for n in neighbors})
        parents.update({n: cur for n in neighbors})
    return distances, parents

def get_tree_info_several(tree:nx.DiGraph, source: str, other_trees: List[nx.DiGraph]):
    distances,parents = dict({source:tree.graph["distances"][source]}),dict()
    infos = [(dict({source:t.graph["distances"][source]}),dict()) for t in other_trees]
    
    to_visit = set([source])
    num_steps = 0
    while len(to_visit) > 0:
        # num_steps += 1
        cur = to_visit.pop()
        # print(num_steps, source)
        distances[cur]
        neighbors = [neighbor for neighbor in get_neighbors(tree, [cur], generator=True) if neighbor not in tree.graph["joined_nodes"]]
        # TODO make update with generator of tuples
        distances.update(((n,distances[cur] + tree.edges[cur, n]['length']) for n in neighbors))
        parents.update(((n,cur) for n in neighbors))
        for i,t in enumerate(other_trees):
            t_distances,t_parents = infos[i]
            t_neighbors = [neighbor for neighbor in get_neighbors(t, [cur], generator=True) if neighbor not in t.graph["joined_nodes"] and neighbor in neighbors] # True or neighbor not in t.graph["joined_nodes"] and
            t_distances.update(((n,t_distances[cur] + t.edges[cur, n]['length']) for n in t_neighbors))
            t_parents.update(((n,cur) for n in t_neighbors))
        to_visit.update(neighbors)
    return distances,parents,infos
            
        


def get_leaves(tree: nx.DiGraph) -> List[str]:
    leaves = [n for n in tree.nodes() if len(get_neighbors(tree, [n])) == 0]
    return leaves



def dump_graph(graph, path):
    data = nx.readwrite.node_link_data(graph)
    json.dump(data, io.open(path, "w"), ensure_ascii=False)


def load_graph(path):
    g = nx.DiGraph()
    json_g = json.load(io.open(path, encoding="utf-8"))
    nodes = [(n["id"], n) for n in json_g["nodes"]]
    edges = [(e["source"], e["target"], e) for e in json_g["links"]]
    g.add_nodes_from(nodes)
    g.add_edges_from(edges)
    return g


def get_pos(g: nx.DiGraph):
    pos = {n: g.nodes[n]['pos'] for n in g.nodes()}
    return pos


def get_reachable_nodes(g, source):
    paths = nx.single_source_shortest_path(g, source)
    return list(paths.keys())

def get_k_neighbors_around_distance(g, node,num_neighbors,neighbor_distance_coef):
    """
    get num_neighbors nodes that are distant to provided node with some coef
    if coed == 0, the closest neighbors are taken
    if coef == 1, the max distant neighbors are taken
    otherwise, taken neighbors are around this coef (half n-s are closer, half n-s are more distant)
    """
    neighbor = [node]
    shortest_paths = nx.shortest_path(g, source=neighbor[0], weight='length')
    if (len(shortest_paths.keys()) < num_neighbors):
        pytest.skip(f"not enough paths: {shortest_paths}<{num_neighbors}")
    # print(shortest_paths)
    del shortest_paths[node]
    sorted_shortest_paths = sorted(shortest_paths.items(), key=lambda item:len(item[1]))
    # print(sorted_shortest_paths)

    max_path_length = len(sorted_shortest_paths[-1][1])
    e_distance_to_other_sources = int(max_path_length * neighbor_distance_coef)
    nearest_nodes = [sp[0] for sp in sorted_shortest_paths if len(sp[1])<=e_distance_to_other_sources]
    distant_nodes = [sp[0] for sp in sorted_shortest_paths[len(nearest_nodes):]]
    neighbor.extend(nearest_nodes[len(nearest_nodes) - (num_neighbors-1)//2:])
    neighbor.extend(distant_nodes[0:(num_neighbors-1)//2])
    if len(neighbor) < num_neighbors:
        already_taken_ind = len(nearest_nodes) - (num_neighbors-1)//2
        diff = num_neighbors - len(neighbor)
        neighbor.extend(nearest_nodes[already_taken_ind - diff:already_taken_ind])
    if len(neighbor) < num_neighbors:
        already_taken_ind = (num_neighbors-1)//2
        diff = num_neighbors - len(neighbor)
        neighbor.extend(distant_nodes[already_taken_ind:already_taken_ind + diff])
    assert len(neighbor) == num_neighbors, (len(neighbor), num_neighbors)
    return neighbor

def build_spt_with_nx(g:nx.DiGraph,source:str):
    shortest_paths = nx.shortest_path(g, source=source, weight='length')
    spt = nx.DiGraph()
    # print(shortest_paths)
    for target, path in shortest_paths.items():
        spt.add_edges_from([(path[i-1],path[i], {'length':g.edges[path[i-1],path[i]]['length']}) for i in range(1,len(path))])
    return spt

def build_path(g:nx.DiGraph,source:str,target:str):
    path = nx.shortest_path(g, source, target, weight='length')
    res = nx.DiGraph()
    res.add_edges_from([(path[i-1],path[i], {'length':g.edges[path[i-1],path[i]]['length']}) for i in range(1,len(path))])
    return res


def get_num_children(tree:nx.DiGraph,source):
    return len(get_tree_info(tree,source)[0].keys())

def force_set_edge(g:nx.DiGraph,node_from,node_to,**kwargs):
    if g.has_edge(node_from,node_to):
        g.edges[node_from,node_to].update(kwargs)
    else:
        g.add_edge(node_from,node_to,**kwargs)
        
def get_edges_on_distance(g: nx.DiGraph, source, distance_coef):
    shortest_paths = nx.shortest_path(g, source=source, weight='length')
    # map paths_lengths to nodes
    # calculate distances to edges
    # calculate appropriate int distances
    # choose appropriate edges
    dst_2_nodes = defaultdict(list)
    for node,path in shortest_paths.items():
        dst_2_nodes[len(path)].append(node)
    max_dst = max(dst_2_nodes.keys())
    
    appropriate_dst = int(max_dst * distance_coef)
    if appropriate_dst == max_dst:
        appropriate_dst -= 1
    if appropriate_dst == max_dst - 1:
        appropriate_dst -= 1

    appropriate_edges = set()
    for length in dst_2_nodes:
        if length > appropriate_dst+1:
            for node in dst_2_nodes[length]:
                edge = (shortest_paths[node][appropriate_dst],shortest_paths[node][appropriate_dst+1])
                appropriate_edges.add(edge)
    return list(appropriate_edges)
    

if __name__ == "__main__":
    g = load_graph('tests/resource\\hex\\hex50-0.json')
    get_edges_on_distance(g,0,0.999)