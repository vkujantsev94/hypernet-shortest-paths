from pickle import FALSE
import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
import spt_lib_v2
from pathlib import Path
import os
import generate_correctness_tests
import generate_time_tests
import generate_complex_tests_single

def run_graph_old():
    resources = "tests/resource/10"
    graph_file = os.path.join(resources, sorted(os.listdir(resources))[0])
    print(graph_file)
    g = util.load_graph(graph_file)
    s = list(g.nodes)[0]
    nx.draw_networkx(g, pos=util.get_pos(g))
    plt.show()
    nodes = list(g.nodes)
    s = nodes[0]
    spt = spt_lib.build_spt(g, s, verbose=True)
    nx.draw_networkx(spt, pos=util.get_pos(g))
    plt.show()


def run_graph(grah_file_path):
    print(grah_file_path)
    g = util.load_graph(grah_file_path)
    s = list(g.nodes)[0]
    g = nx.subgraph(g, util.get_reachable_nodes(g, s))
    
    nx.draw_networkx(g, pos=nx.get_node_attributes(g, 'pos'))
    # print("{:.2f}".format(a))
    labels = nx.get_edge_attributes(g,'length')
    labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
    nx.draw_networkx_edge_labels(g, pos=util.get_pos(g), edge_labels=labels)
    plt.show()
    nodes = list(g.nodes)
    s = nodes[0]
    spt = spt_lib.build_spt(g, s, debug=True)
    actual = nx.shortest_path_length(spt, source=s, weight='length')
    expected = nx.shortest_path_length(g, source=s, weight='length')
    actual_path = nx.shortest_path(spt, source=s, weight='length')
    expected_path = nx.shortest_path(g, source=s, weight='length')
    for node in actual:
        print(f"node {node}:")
        if expected[node] != actual[node]: print("\tERROR")
        print(f"\tlength: expected={expected[node]}, actual={actual[node]}")
        print(f"\tpath: expected={expected_path[node]}, actual={actual_path[node]}")
    # print("expected", expected)
    # print("actual", actual)
    nx.draw_networkx(spt, pos=util.get_pos(g))
    labels = nx.get_edge_attributes(spt,'length')
    labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
    nx.draw_networkx_edge_labels(spt, pos=util.get_pos(g), edge_labels=labels)
    plt.show()

def run_graph_several(grah_file_path):
    g = util.load_graph(grah_file_path)
    s = list(g.nodes)[0]
        
    print(nx.shortest_path_length(g, source=s, weight='length'))
    sources = util.get_k_neighbors_around_distance(g,s,2,0)
    spts = spt_lib.build_several_spts(g,sources,debug=True)
    for spt in spts:
        nx.draw_networkx(spt, pos=util.get_pos(g))
        labels = nx.get_edge_attributes(spt,'length')
        labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
        nx.draw_networkx_edge_labels(spt, pos=util.get_pos(g), edge_labels=labels)
        plt.show()
    # general_str = nx.disjoint_union_all(spts)
    # labels = nx.get_edge_attributes(general_str,'length')
    # labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
    # nx.draw_networkx_edge_labels(general_str, pos=util.get_pos(g), edge_labels=labels)
    # plt.show()

def run_test_sev_remeains(graph_file, num_sources, dist_coef, edge_portion):
    generate_correctness_tests.test_several_spts_remains(graph_file, num_sources, dist_coef, edge_portion)

def measure_time_atomic(g:nx.Graph,source,edge_dist_coef):
    g = nx.Graph(g)
    result = {"node": source, "e_dist": edge_dist_coef, "t_scratch": None, "t_prebuilt": None, "t_remains": None}
    old_spt = util.build_spt_with_nx(g,source)
    
    possible_edges_to_remove = util.get_edges_on_distance(old_spt,source,edge_dist_coef)
    
    # edge_to_remove = possible_edges_to_remove[np.random.randint(len(possible_edges_to_remove))]
    edge_to_remove = max(possible_edges_to_remove, key=lambda e:util.get_num_children(old_spt,e[1]))
    
    print("EDGE TO REMOVE", edge_to_remove)
    g.remove_edge(*edge_to_remove)
    old_spt.remove_edge(*edge_to_remove)
    
    print("===REMAINS===")
    remains = old_spt
    start = time.time()
    spt_lib_v2.build_spt(g,source,spt_remains=remains,optimal=True, debug=False)
    t_remains = time.time() - start
    result["t_remains"] = t_remains

#### DEBIG time of optimal alg
    # "1,resource\hex\100\hex100-0.json,13,0.09507143064099162,0.0020017623901367188,0.002999544143676758,0.004989147186279297,False"    
    # g = util.load_graph(r"tests\resource\hex\100\hex100-0.json")
    # keys = [
    # "elapsed_backup",
    # "elapsed_initial_structs",
    # "total_elapsed_tree_checking",
    # "n_attached",
    # "total_elapsed_attaching",
    # "total_elapsed_neighbor_search",
    # "elapsed_main",
    # "elapsed_final_building",
    # "total_elaped"
    # ]
    # init_measures = {}
    # for k in keys:
    #     init_measures[k] = []
    # json.dump(init_measures, open(f"measure_time.json", "w"))
    
    # r = generate_complex_tests_single.measure_time_atomic(g, 13,0.09507143064099162,n_tests=1)
    # print(r)    

def measure_distance_error_atomic(g: nx.DiGraph, source, edge_dist_coef):
    g = nx.Graph(g)
    results = {"source":[],"target":[],"is_initial":[],"e_dist":[],"lower_bound":[],"upper_bound":[],"approx_length":[],"optimal_length":[],"percent":[],"error":[],"success":[],"o-lb":[]}
    old_spt = util.build_spt_with_nx(g,source)
    

    
    # old_optimal_paths = nx.shortest_path(old_spt, source, weight="length")
    # old_optimal_lengths = nx.shortest_path_length(old_spt, source, weight="length")
    
    possible_edges_to_remove = util.get_edges_on_distance(old_spt,source,edge_dist_coef)
    
    # edge_to_remove = possible_edges_to_remove[np.random.randint(len(possible_edges_to_remove))]
    edge_to_remove = max(possible_edges_to_remove, key=lambda e:util.get_num_children(old_spt,e[1]))
    
    target = 7
    rg = nx.DiGraph()
    rg.add_node(edge_to_remove[0])
    rg.add_node(edge_to_remove[1])
    
    nx.draw_networkx(g, pos=util.get_pos(g))
    nx.draw_networkx(rg, pos=util.get_pos(g), node_color="red")
    nx.draw_networkx(old_spt, pos=util.get_pos(g), edge_color="yellow") #, alpha=0.5
    nx.draw_networkx(util.build_path(old_spt,source,target), pos=util.get_pos(g), edge_color="red") #, alpha=0.5
    
    plt.show()
    plt.cla()
    
    # print("EDGE TO REMOVE", edge_to_remove)
    g.remove_edge(*edge_to_remove)
    old_spt.remove_edge(*edge_to_remove)
    
    remains = old_spt
    distances, _ = util.get_tree_info(old_spt, source)
    initial_nodes = set(distances.keys())
    
    spt = spt_lib_v2.build_spt(g,source,spt_remains=remains,optimal=False)
    print("SPT bad edges", spt.graph["non_optimal_edges"])
    
    optimal_spt = util.build_spt_with_nx(g,source)
    

    nx.draw_networkx(g, pos=util.get_pos(g))
    nx.draw_networkx(rg, pos=util.get_pos(g), node_color="red")
    nx.draw_networkx(util.build_path(spt,source,target), pos=util.get_pos(g), edge_color="blue") #, alpha=0.5
    
    plt.show()
    plt.cla()
    nx.draw_networkx(g, pos=util.get_pos(g))
    nx.draw_networkx(rg, pos=util.get_pos(g), node_color="red")
    nx.draw_networkx(util.build_path(optimal_spt,source,target), pos=util.get_pos(g), edge_color="green") #, alpha=0.5
    plt.show()

    
    
    optimal_lengths = nx.shortest_path_length(optimal_spt,source,weight="length")
    # print("OPTIMAL SPT")
    target_nodes = g.nodes() - set([source])
    for target in target_nodes:
        lower_bound, upper_bound = spt_lib_v2.estimate_path_optimality(source,target,spt)
        # print("ESTIMATE")
        approx_length = upper_bound
        optimal_length = optimal_lengths[target]
        error = approx_length - optimal_length
        success = lower_bound <= optimal_length <= upper_bound
        results["source"].append(source)
        results["target"].append(target)
        results["is_initial"].append(source in initial_nodes)
        results["e_dist"].append(edge_dist_coef)
        results["lower_bound"].append(lower_bound)
        results["upper_bound"].append(upper_bound)
        results["approx_length"].append(approx_length)
        results["optimal_length"].append(optimal_length)
        percent = (upper_bound - lower_bound) / upper_bound
        results["percent"].append(percent)
        results["error"].append(error)
        results["success"].append(success)
        results["o-lb"].append(abs(optimal_length - lower_bound))
    return results
### DRAW graph, sol and opt sol
    # nx.draw_networkx(g, pos=util.get_pos(g))
    # nx.draw_networkx(spt, pos=util.get_pos(g), edge_color=(0,0,1), alpha=0.5)
    # nx.draw_networkx(optimal_spt, pos=util.get_pos(g), edge_color=(0,1,0), alpha=0.5)
    # rg = nx.DiGraph()
    # rg.add_node(edge_to_remove[0])
    # rg.add_node(edge_to_remove[1])
    # nx.draw_networkx(rg, pos=util.get_pos(g), node_color=(1,0,0))

if __name__ == "__main__":
    """
    1446,resource\hex\10\hex10-112.json,5,7,True,0.03677831327192532,4.4639999999999995,5.4639779997579945,5.4639779997579945,3.7319779997579947,0.1830128159012143,1.7319999999999998,False,0.7320220002420048
    """
    g = util.load_graph(r"tests\resource\hex\10\hex10-112.json")
    r = measure_distance_error_atomic(g,5,0.03677831327192532)
    # print(r)
    plt.show()
