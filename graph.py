import os
import random
import tkinter as tk
import json
from collections import deque
from itertools import islice
from typing import List

from PIL import Image, ImageTk
import io

from edge import Edge
from fuzzy import FuzzyNumber
from geom import Point, Vector
from util import *


class Graph:
    def __init__(self, *args):
        self.graph = [[]]
        self.source = 0
        self.sink = 0
        self.num_vertices = 0
        self.num_edges = 0
        if len(args) == 0:
            return
        file = args[0]
        if ".json" in file:
            with open(file) as json_file:
                data = json.load(json_file)
                self.num_vertices = data['num_vertices']
                # self.num_edges = data['num_edges']
                self.source = data['source']
                self.sink = data['sink']
                self.graph = [[] for i in range(self.num_vertices)]
                json_edges = data['edges']
                for js_edge in json_edges:
                    self.add_edge(Edge(js_edge))
            return
        with open(file) as f:
            n = int(f.readline())
            self.num_vertices = n
            self.graph = [[] for i in range(n)]

            m = int(f.readline())
            self.num_edges = m
            self.source = int(f.readline())
            self.sink = int(f.readline())
            # TODO fix stupid line reading
            for i in range(m):
                if i == 3:
                    pass
                line = f.readline().split()
                assert len(line) == 2, "1st line must have exactly 2 numbers - vertices ids"
                vertices = to_int(line)
                v_from, v_to = vertices[0], vertices[1]

                line = f.readline().split(',')
                assert len(line) == 3, "2nd line must have exactly 3 numbers - fuzzy number for a flow"
                fl = to_float(line)
                flow = FuzzyNumber(fl[0], fl[1], fl[2])

                line = f.readline().split(',')
                assert len(line) == 3, "3nd line must have exactly 3 numbers - fuzzy number for a capacity"
                cap = to_float(line)
                capacity = FuzzyNumber(cap[0], cap[1], cap[2])

                w = int(f.readline())
                new_edge = Edge(v_from,
                                v_to,
                                capacity,
                                flow,
                                w)
                self.graph[v_from].append(new_edge)

    def add_edge(self, edge):
        edge_copy = edge.__copy__()
        self.graph[edge.vert_from].append(edge_copy)
        self.num_edges += 1

    def remove_edge(self, uv):
        if not isinstance(uv, tuple):
            assert False
        e = self[uv]
        self.graph[e.vert_from].remove(e)

    def __getitem__(self, item) -> List[Edge]:
        if isinstance(item, tuple):
            assert len(item) == 2
            edge_list = self.graph[item[0]]
            for e in edge_list:
                if e.vert_to == item[1]:
                    return e
            assert False, "No edge found"
        return self.graph[item]

    def __copy__(self):
        """
        Makes a copy of graph. Edges are the same objects.
        :return: copy
        """
        copy_instance = Graph()
        copy_instance.num_vertices = self.num_vertices
        copy_instance.num_edges = self.num_edges
        copy_instance.source = self.source
        copy_instance.sink = self.sink

        n = self.num_vertices
        copy_instance.graph = [[] for i in range(n)]
        for i in range(n):
            for j in range(len(self[i])):
                copy_instance[i].append(self[i][j])
        return copy_instance

    def deep_copy(self):
        """
        Makes deep copy of graph.
        :return: deep copy
        """
        copy_instance = Graph()
        copy_instance.num_vertices = self.num_vertices
        copy_instance.num_edges = self.num_edges
        copy_instance.source = self.source
        copy_instance.sink = self.sink

        n = self.num_vertices
        copy_instance.graph = [[] for i in range(n)]
        for i in range(n):
            for j in range(len(self[i])):
                copy_instance[i].append(self[i][j].__copy__())
        return copy_instance

    def drop_edges(self):
        for edge_list in self.graph:
            edge_list.clear()
        self.num_edges = 0

    def drop_reversed_edges(self):
        edges_to_remove = [[] for i in range(self.num_vertices)]
        for i in range(self.num_vertices):
            for edge in self.graph[i]:
                if edge.is_reversed:
                    edges_to_remove[i].append(edge)
        for i in range(self.num_vertices):
            for edge in edges_to_remove[i]:
                self.graph[i].remove(edge)
                self.num_edges -= 1

    def total_flow_log_entropy(self):
        total_l_e = 0
        for edge_list in self.graph:
            for edge in edge_list:
                if edge.is_reversed:
                    continue
                total_l_e += edge.flow.log_entropy_value()
        return total_l_e

    def get_graph_skeleton(self, val=None):
        return [[val for edge in edge_list] for edge_list in self.graph]

    def get_edge_iterator(self, with_reversed=True):
        def edge_iterator():
            for i in range(self.num_vertices):
                for edge in self.graph[i]:
                    if edge.is_reversed:
                        if with_reversed:
                            yield edge
                    else:
                        yield edge
        return edge_iterator()

    def eval_source_flow(self):
        return sum((e.flow for e in self.graph[self.source]))

    def print_config_file(self, output=None):
        res = list()
        res.append(str(self.num_vertices))
        res.append(str(self.num_edges))
        res.append(str(self.source))
        res.append(str(self.sink))
        for i in range(self.num_vertices):
            for j in range(len(self.graph[i])):
                res.append(str(self.graph[i][j]))

        res_str = "\n".join(res)
        if output:
            with open(os.path.join(output), 'w') as out:
                out.write(res_str)
        else:
            print(res_str)

    def print_pretty(self, output=None):
        if output:
            with open(os.path.join(output), 'w') as out:
                out.write(self.pretty())
        else:
            print(self.pretty())

    def pretty(self):
        res = list()
        res.append("num_vert = " + str(self.num_vertices))
        res.append("num_edges = " + str(self.num_edges))
        res.append("source: " + str(self.source))
        res.append("sink: " + str(self.sink))
        for i in range(self.num_vertices):
            res.append(str(i) + ":")
            for edge in self.graph[i]:
                res.append("\t " + edge.pretty())
        return "\n".join(res)

    def vert_dist(self, vert_f, vert_s):
        path = bfs(self, vert_f, vert_s)
        if path is not None:
            return len(path)
        return float('inf')

    def edge_dist(self, edge_f, edge_s):
        return self.vert_dist(edge_f.vert_to, edge_s.vert_from)

    def visualise(self, schema=None, window_size=600):
        gd = GraphDrawer(self, schema, window_size)
        gd.set_title("OH MY GOD ITS A GRAPH")
        gd.visualize()

    def to_json(self):
        d = dict()
        d['num_vertices'] = self.num_vertices
        d['num_edges'] = self.num_edges
        d['source'] = self.source
        d['sink'] = self.sink
        d['edges'] = []
        edges = d['edges']
        for e in self.get_edge_iterator(with_reversed=False):
            edges.append(e.to_json())
        return d

    def dump(self, path):
        with open(path, 'w') as out:
            json.dump(self.to_json(), out)

    def to_dot(self):
        res_str = ""
        res_str += "digraph G {\n"
        for e in self.get_edge_iterator(with_reversed=False):
            res_str += f'{e.vert_from} -> {e.vert_to}'
            res_str += ' [label="'
            res_str += f'<{e.flow}>'
            res_str += r'\n'
            res_str += f'<{e.capacity}>"'
            if e.mark == True:
                res_str += ' color = "red"'
            res_str += ']'
            res_str += '\n'
        res_str += "}\n"
        return res_str

    def is_fuzzy(self):
        for edge in self.get_edge_iterator():
            if edge.flow.log_entropy_value() > 0:
                return True
        return False


class GraphDrawer:
    # TODO переделать на вызов GraphViz (для начала реальзовать и сравнить)
    def __init__(self, g: Graph, schema=None, window_size=600):
        self.graph = g
        self.num_vert_in_row = None
        self.title = None
        self.num_vert_in_row = int((self.graph.num_vertices - 2) ** (1 / 2)) + 1
        self.window_size = window_size
        if schema is None:
            self.vert_coords = self._init_vertices_coords_random()
        else:
            self.vert_coords = self._init_vert_coords_by_schema(schema)
        self.vert_canvas_coords = self._scale_coords()
        self.vert_radius = 10

    def visualize(self):
        root = tk.Tk()
        size = self.window_size
        canvas = tk.Canvas(root, width=size, height=size)
        canvas.pack()
        self._visualize_edges(canvas)
        self._visualize_vertices(canvas)
        if self.title:
            canvas.create_text(size / 2, 10, text=self.title)
            self.title = None
        root.update()

        def exit(event): root.destroy()

        root.bind('e', exit)
        root.mainloop()

    def set_title(self, t):
        self.title = t

    def save_canvas_snapshot(self, gif_resource_path, canvas, num):
        width = canvas['width']
        height = canvas['height']
        # ps = self.canvas.postscript(
        #     file=os.path.join(gif_resource_path, "test.eps"),
        #     colormode='color', pagewidth=width, pageheight=height, width=width, height=height)

        # self.canvas.postscript(
        #     file=os.path.join(gif_resource_path, "circles.eps"),
        #     colormode='color', pagewidth=width, pageheight=height, width=width, height=height)
        ps = canvas.postscript(
            # file=os.path.join(gif_resource_path, "circles.eps"),
                colormode='color', pagewidth=width, pageheight=height, width=width, height=height)
        pil_image = Image.open(io.BytesIO(ps.encode('utf-8')))
        # pil_image.show()
        name = "ss" + str(num).rjust(7, '0') + ".png"
        snapshot_path = os.path.join(gif_resource_path, name)
        pil_image.save(snapshot_path)

    def _visualize_edges(self, canvas: tk.Canvas):
        for edge in self.graph.get_edge_iterator():
            if edge.is_reversed:
                continue
            x1, y1 = self.vert_canvas_coords[edge.vert_from]
            x2, y2 = self.vert_canvas_coords[edge.vert_to]
            vec_x, vec_y = (x2 - x1), (y2 - y1)
            vec_xx = vec_x / (vec_x ** 2 + vec_y ** 2) ** (1 / 2) * self.vert_radius
            vec_yy = vec_y / (vec_x ** 2 + vec_y ** 2) ** (1 / 2) * self.vert_radius
            canvas.create_line(x1, y1, x2 - vec_xx, y2 - vec_yy, arrow=tk.LAST)

            middle = x1 + vec_x / 2, y1 + vec_y / 2
            canvas.create_text(middle[0], middle[1] - self.vert_radius, text=edge.flow)
            canvas.create_text(*middle, text=edge.capacity)

    def _visualize_vertices(self, canvas: tk.Canvas):
        for i, vc in enumerate(self.vert_canvas_coords):
            x = vc[0]
            y = vc[1]
            canvas.create_oval(x - self.vert_radius, y - self.vert_radius,
                               x + self.vert_radius, y + self.vert_radius, fill='white')
            canvas.create_text(x, y, text=str(i))

    def _scale_coords(self):
        # bounding points of picture
        # TODO get rid of magic constant. Area of visualized graph depends on vertex size
        min_abs_bp = Point(self.window_size * 0.1, self.window_size * 0.1)
        max_abs_bp = Point(self.window_size * 0.9, self.window_size * 0.9)
        dx = (max_abs_bp.x - min_abs_bp.x) / self.graph_x_size
        dy = (max_abs_bp.y - min_abs_bp.y) / self.graph_y_size
        self._get_coords_bounds(self.vert_coords)
        res = []
        for vc in self.vert_coords:
            x = min_abs_bp.x + vc[0] * dx
            y = min_abs_bp.y + vc[1] * dy
            res.append((x, y))
        return res

    def _get_coords_bounds(self, coords):
        min_x, min_y = coords[0]
        max_x, max_y = coords[0]
        for vc in coords[1:]:
            min_x = min(min_x, vc[0])
            min_y = min(min_y, vc[1])
            max_x = max(max_x, vc[0])
            max_y = max(max_y, vc[1])
        return Point(min_x, min_y), Point(max_x, max_y)

    def _init_vert_coords_by_schema(self, schema):
        # consider schema is rectangular
        vertices_coords = [None] * self.graph.num_vertices
        with open(schema) as f:
            s = f.readline()
            i = 0
            while s != "":
                for j, char in enumerate(s):
                    if char == '#' or char == '\n':
                        continue
                    vert = int(char)
                    vertices_coords[vert] = (j, i)
                s = f.readline()
                i += 1
        for vc in vertices_coords:
            if vc is None:
                raise RuntimeError("Could not resolve schema.")
        self.graph_x_size = j + 1
        self.graph_y_size = i + 1
        return vertices_coords

    def _init_vertices_coords_random(self):
        graph = self.graph
        s, t, n, m = graph.source, graph.sink, graph.num_vertices, graph.num_edges

        collision_count = m * m
        iter_count = 0
        while collision_count > 0:
            iter_count += 1
            assert iter_count < 10000
            vertices_coords = self._shuffle()
            if not self._check_no_vertices_on_edges(vertices_coords):
                continue
            collision_count = 0
            for edge_number, edge_f in enumerate(graph.get_edge_iterator()):
                p_from = Point(*vertices_coords[edge_f.vert_from])
                p_to = Point(*vertices_coords[edge_f.vert_to])
                v1 = Vector(p_from, p_to)
                for edge_s in islice(graph.get_edge_iterator(), edge_number + 1, m):
                    if edge_f.touches(edge_s):
                        continue
                    p_from = Point(*vertices_coords[edge_s.vert_from])
                    p_to = Point(*vertices_coords[edge_s.vert_to])
                    v2 = Vector(p_from, p_to)
                    if v1.collides(v2):
                        collision_count += 1
        p_min, p_max = self._get_coords_bounds(vertices_coords)
        self.graph_x_size = p_max.x
        self.graph_y_size = p_max.y
        return vertices_coords

    def _shuffle(self):
        vert_coords = []
        set_of_coords = set()
        x = random.randint(1, self.num_vert_in_row)
        y = random.randint(0, self.num_vert_in_row)
        for i in range(self.graph.num_vertices):
            if i == self.graph.source:
                vert_coords.append((0, self.num_vert_in_row / 2))
                continue
            if i == self.graph.sink:
                vert_coords.append((self.num_vert_in_row + 1, self.num_vert_in_row / 2))
                continue
            while (x, y) in set_of_coords:
                x = random.randint(1, self.num_vert_in_row)
                y = random.randint(0, self.num_vert_in_row)
            vert_coords.append((x, y))
            set_of_coords.add((x, y))
        return vert_coords

    def _check_no_vertices_on_edges(self, vert_coords):
        for i, coord in enumerate(vert_coords):
            vertex_point = Point(*coord)
            for edge in self.graph.get_edge_iterator():
                if i == edge.vert_from or i == edge.vert_to:
                    continue
                p_from = Point(*vert_coords[edge.vert_from])
                p_to = Point(*vert_coords[edge.vert_to])
                if Vector(p_from, p_to).contains_point(vertex_point):
                    return False
        return True


##############################################


def bfs(gr: Graph, s, t, cond=None) -> List[Edge]:
    queue = deque()
    queue.append(s)
    visited = [-1] * gr.num_vertices

    iter_counter = 0
    max_iter = 10000

    while len(queue) > 0:
        iter_counter += 1
        assert iter_counter < max_iter, "BFS max iteration count exceeded."
        u = queue.popleft()
        for edge in gr[u]:
            if visited[edge.vert_to] == -1 and edge.vert_to != s:
                if cond is not None:
                    if not cond(edge):
                        continue
                visited[edge.vert_to] = edge
                queue.append(edge.vert_to)
            if edge.vert_to == t:
                break
    path = list()
    if visited[t] != -1:
        u = t
        while visited[u] != -1:
            path.append(visited[u])
            u = visited[u].vert_from
        assert u == s, "BFS path does not contain the source."
        return [x for x in reversed(path)]
    return None


def calc_path_distance(path_as_edges: List[Edge]):
    d = 0
    for edge in path_as_edges:
        d += edge.weight
    return d


def str_path(path: List[Edge]):
    res = [str(path[0].vert_from)]
    for edge in path:
        res.append("->")
        res.append(str(edge.vert_to))
    return "".join(res)


def ford_bellman(gr: Graph, s, t):
    """
    Ford-Bellman algorithm for finding lowest cost path from s to t
    :param gr: graph
    :param s: source
    :param t: sink
    :return: Shortest tour length, shortest tour path. Path is represented by graph vertices
    """
    gr_copy = gr.__copy__()
    # s, t = gr_copy.source, gr_copy.sink
    n = gr_copy.num_vertices
    d = [INF] * n
    d[s] = 0
    p = [-1] * n
    while True:
        any_ = False
        for i in range(n):
            for j in range(len(gr_copy[i])):
                cur_edge = gr_copy[i][j]
                if d[cur_edge.vert_from] < INF:
                    if d[cur_edge.vert_to] > d[cur_edge.vert_from] + cur_edge.weight:
                        d[cur_edge.vert_to] = d[cur_edge.vert_from] + cur_edge.weight
                        p[cur_edge.vert_to] = cur_edge.vert_from
                        any_ = True
        if not any_:
            break
    if d[t] == INF:
        raise RuntimeError("No path found")
    # проверять путь на наличие ненасыщенного помеченного ребра. если такого нет, то?
    # как вариант, если путь не тот, то добавлять всем ребрам на текущем полученном пути + K к стоимости
    path = []
    cur_v = t
    while cur_v != -1:
        path.append(cur_v)
        cur_v = p[cur_v]
    path = list(reversed(path))
    # возвращать путь, как ребра за O(m) полным обходом(что, если есть кратные ребра?)
    return d[t], path


def convert_path_to_edges(gr: Graph, path):
    """
    Tries to build path as graph edges from vertices.
    If such path cant be made, an exception will be thrown.
    :param gr: graph
    :param path: path of vertices
    :return: path of edges from graph.
    """
    path_edges = []
    for i, vertex in enumerate(path[0:-1]):
        next_vertex = path[i + 1]

        found = False
        for edge in gr[vertex]:
            if edge.vert_to == next_vertex:
                path_edges.append(edge)
                found = True
        if found:
            continue
        raise RuntimeError(f"Couldn't find corresponding edge for vertices ({vertex},{next_vertex})")
    return path_edges
