
import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest
import itertools

import generate_time_tests


resource = os.path.join("resource", "1000")
tests = [os.path.join(resource, t) for t in sorted(os.listdir(resource))][0:1]
tests = ['resource\\1000\\graph1000-0']


@pytest.mark.time
@pytest.mark.nn1000
@pytest.mark.close_vs_distant
# @pytest.mark.parametrize(
#     'args', zip(tests, itertools.repeat([0,1]), itertools.repeat(2), itertools.repeat(0.1))
# )
def test_g():
    assert generate_time_tests.test_several_paths(tests,[0,1],2,0.1)


# def test(self, args):
#     assert generate_correctness_tests.test_spt_with_remains(*args)
