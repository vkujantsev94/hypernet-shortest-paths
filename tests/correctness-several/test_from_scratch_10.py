import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest

import generate_correctness_tests


resource = os.path.join("resource", "10")
tests = [os.path.join(resource, t) for t in sorted(os.listdir(resource))]#[:10]


@pytest.mark.correctness
@pytest.mark.nn10
@pytest.mark.ns2
@pytest.mark.close
@pytest.mark.scratch
@pytest.mark.parametrize(
    'graph_file', tests
)
def test_1(graph_file):
    assert generate_correctness_tests.test_several_spts_scratch(graph_file,2,0)    


@pytest.mark.correctness
@pytest.mark.nn10
@pytest.mark.ns2
@pytest.mark.distant
@pytest.mark.scratch
@pytest.mark.parametrize(
    'graph_file', tests
)
def test_2(graph_file):
    assert generate_correctness_tests.test_several_spts_scratch(graph_file,2,1)  
