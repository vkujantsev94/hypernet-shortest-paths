import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))


import itertools
import pytest
import numpy as np
import generate_correctness_tests


resource = os.path.join("resource", "100")
tests = [os.path.join(resource, t) for t in sorted(os.listdir(resource))]#[5:6]
np.random.seed(42)
edge_portions = list(np.random.rand(len(tests)))


@pytest.mark.correctness
@pytest.mark.nn100
@pytest.mark.remains
@pytest.mark.several
@pytest.mark.close
@pytest.mark.parametrize(
    #           graph  num_sources          distance_coef        edges_in_prebuilt_coef
    'args', zip(tests, itertools.repeat(2), itertools.repeat(0), edge_portions)
)
def test1(args):
    assert generate_correctness_tests.test_several_spts_remains(*args)


@pytest.mark.correctness
@pytest.mark.nn100
@pytest.mark.remains
@pytest.mark.several
@pytest.mark.distant
@pytest.mark.parametrize(
    #           graph  num_sources          distance_coef        edges_in_prebuilt_coef
    'args', zip(tests, itertools.repeat(2), itertools.repeat(1), edge_portions)
)
def test2(args):
    assert generate_correctness_tests.test_several_spts_remains(*args)
