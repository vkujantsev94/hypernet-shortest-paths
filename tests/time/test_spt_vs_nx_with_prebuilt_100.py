import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest

import generate_time_tests


resource = os.path.join("resource", "100")
tests = [os.path.join(resource, t) for t in sorted(os.listdir(resource))]#[0:1]
np.random.seed(42)
edge_portions = list(np.random.rand(len(tests)))

@pytest.mark.time
@pytest.mark.nn100
@pytest.mark.prebuilt
def test_g():
    assert generate_time_tests.test_spt_vs_nx_with_prebuilt(tests, edge_portions)
