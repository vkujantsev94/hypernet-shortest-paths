import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest

import generate_time_tests


resource = os.path.join("resource", "100")
tests = [os.path.join(resource, t) for t in sorted(os.listdir(resource))]#[0:1]

@pytest.mark.time
@pytest.mark.nn100
@pytest.mark.scratch
def test_g():
    assert generate_time_tests.test_spt_vs_nx_from_scratch(tests)
