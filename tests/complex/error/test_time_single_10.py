import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest

import generate_complex_tests_single


resource = os.path.join("resource", "hex","10")
graph_files = [os.path.join(resource, t) for t in sorted(os.listdir(resource))]#[0:10]
# np.random.seed(42)
# edge_portions = list(np.random.rand(len(graph_files)))


@pytest.mark.time
@pytest.mark.nn10
@pytest.mark.complex
def test():
    generate_complex_tests_single.__error_test__(graph_files, 1, 10, random_seed=42, out_file="error-10.csv")
