import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest

import generate_correctness_tests


resource_10 = os.path.join("resource", "10")
tests_10 = [os.path.join(resource_10, t) for t in sorted(os.listdir(resource_10))]#[0:1]


class Test_10():
    @pytest.mark.correctness
    @pytest.mark.nn10
    @pytest.mark.scratch
    @pytest.mark.parametrize(
        'graph_file', tests_10
    )
    def test_g(self, graph_file):
        assert generate_correctness_tests.test_spt_from_scratch(graph_file)
        
