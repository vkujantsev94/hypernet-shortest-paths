import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
from pathlib import Path
import os
import pytest
import re

import generate_correctness_tests



resource_1000 = os.path.join("resource", "1000")
tests_1000 = [os.path.join(resource_1000, t) for t in sorted(os.listdir(resource_1000))]



class Test_1000():
    @pytest.mark.correctness
    @pytest.mark.nn1000
    @pytest.mark.scratch
    @pytest.mark.parametrize(
        'graph_file', tests_1000
    )
    def test_g(self, graph_file):
        assert generate_correctness_tests.test_spt_from_scratch(graph_file)
