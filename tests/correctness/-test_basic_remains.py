import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
from pathlib import Path
import os
import pytest
import util
import spt_lib
import generate_correctness_tests


resource = os.path.join("resource", "basic")
# tests_10 = [os.path.join(resource_10, t) for t in sorted(os.listdir(resource_10))]#[0:1]



# @pytest.mark.basic
# def test_b():
#     graph_file = resource + "/basic_22.json"
#     g = util.load_graph(graph_file)
#     source=0
#     pre_spt = util.build_spt_with_nx(g,source)
#     edge_to_remove = (2,3)
#     g.remove_edge(*edge_to_remove)
#     pre_spt.remove_edge(*edge_to_remove)
#     spt = spt_lib.build_spt(g,source,pre_spt,debug=True)
#     assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
#     actual = nx.shortest_path_length(spt, source=source, weight='length')
#     expected = nx.shortest_path_length(g, source=source, weight='length')
#     assert actual == expected, "path lengths are not optimal"

@pytest.mark.basic
def test_grid_1():
    graph_file = resource + "/grid_4x5.json"
    g = util.load_graph(graph_file)
    
    nx.draw_networkx(g, pos=util.get_pos(g))
    labels = nx.get_edge_attributes(g,'length')
    labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
    nx.draw_networkx_edge_labels(g, pos=util.get_pos(g), edge_labels=labels)
    plt.show()
    
    source=0
    pre_spt = util.build_spt_with_nx(g,source)
    edge_to_remove = (0,6)
    g.remove_edge(*edge_to_remove)
    pre_spt.remove_edge(*edge_to_remove)
    spt = spt_lib.build_spt(g,source,pre_spt,debug=True)
    assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
    actual = nx.shortest_path_length(spt, source=source, weight='length')
    expected = nx.shortest_path_length(g, source=source, weight='length')
    if actual != expected:
        actual_path = nx.shortest_path(spt, source=source, weight='length')
        expected_path = nx.shortest_path(g, source=source, weight='length')
        for node in sorted(actual.keys()):
            print(f"node {node}:")
            if expected[node] != actual[node]: print("\tERROR")
            print(f"\tlength: expected={expected[node]}, actual={actual[node]}")
            print(f"\tpath: expected={expected_path[node]}, actual={actual_path[node]}")
        true_spt = util.build_spt_with_nx(g,source)
        while True:
            for gg in [pre_spt, spt, true_spt]:
                nx.draw_networkx(gg, pos=util.get_pos(g))
                labels = nx.get_edge_attributes(gg,'length')
                labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
                nx.draw_networkx_edge_labels(gg, pos=util.get_pos(g), edge_labels=labels)
                plt.show()
    assert actual == expected, "path lengths are not optimal"