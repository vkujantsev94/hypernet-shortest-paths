import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import pytest
import numpy as np
import generate_correctness_tests
import itertools


resource_100 = os.path.join("resource", "100")
tests_100 = [os.path.join(resource_100, t) for t in sorted(os.listdir(resource_100))]#[0:10]
np.random.seed(42)
edge_portions = list(np.random.rand(len(tests_100)))


class Test():
    @pytest.mark.correctness
    @pytest.mark.nn100
    @pytest.mark.prebuilt
    @pytest.mark.parametrize(
        'args', zip(tests_100, itertools.repeat((0.2)))
    )
    # @pytest.mark.parametrize(
    #     'edge_portion', edge_portions
    # )
    def test(self, args):
        assert generate_correctness_tests.test_spt_with_prebuilt(*args)
        
