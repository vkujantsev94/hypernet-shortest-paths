import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'generate_tests')))
sys.path.append(os.path.abspath(os.path.join('..')))



import pytest
import numpy as np
import generate_correctness_tests
import itertools


resource_1000 = os.path.join("resource", "1000")
tests_1000 = [os.path.join(resource_1000, t) for t in sorted(os.listdir(resource_1000))]#[0:1]
np.random.seed(42)
edge_portions = list(np.random.rand(len(tests_1000)))


class Test():
    @pytest.mark.correctness
    @pytest.mark.nn1000
    @pytest.mark.remains
    @pytest.mark.parametrize(
        'args', zip(tests_1000, itertools.repeat((0.2)))
    )
    # @pytest.mark.parametrize(
    #     'edge_portion', edge_portions
    # )
    def test(self, args):
        assert generate_correctness_tests.test_spt_with_remains(*args)
        
