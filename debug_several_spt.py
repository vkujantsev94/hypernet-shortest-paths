from collections import defaultdict
import util
import time

from datetime import date

logfilepath = "debug_several.log"
with open(logfilepath, "w") as f:
    f.write("")
def log(*args,filepath=logfilepath):
    print(*args)
    print(*args, file=open(filepath, "a"))
    
    

graph_files = ['tests\\resource\\1000\\graph1000-0']
source_distance_coefs = [0,1]
num_sources = 2

if __name__ == "__main__":
    log(date.today())
    times = defaultdict(list)
    # print(1)
    for gf in graph_files:
        g = util.load_graph(gf)
        log(f"{g}")
        for sdc in source_distance_coefs:
            log("=========")
            log(f"sdc {sdc}")
            first_source = list(g.nodes)[0]
            sources = util.get_k_neighbors_around_distance(g,first_source,num_sources,sdc)
            log(f"sources: {sources}")
            log("---")
            prebuilt_spts = [util.build_spt_with_nx(g, s) for s in sources]
            f_spt = prebuilt_spts[0]
            f_source = sources[0]
            neighbors = list(util.get_neighbors(f_spt, [f_source]))
            log(neighbors)
            # log(f_spt[f_source].keys())
            num_children = [(n,util.get_num_children(f_spt, n)) for n in sorted(neighbors)]
            log(num_children)
            
            max_ch = max(num_children, key=lambda ch: ch[1])
            log(max_ch)
            log(f_source)
            log(f_spt.edges[0,672])
            
                
            # log(util.get_tree_info(f_spt, f_source))
            
            
            