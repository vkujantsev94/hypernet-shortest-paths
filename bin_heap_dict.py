from iheap import iHeap
import heapq
from heapdict import heapdict
from typing import Optional,Iterable


class BinHeapDict(iHeap):
    def __init__(self, items:Optional[Iterable]=None):
        """
        O(n)
        """
        initial_items = dict()
        if items is not None:
            initial_items = {item[1]: item[0] for item in items}
        self.data = heapdict(initial_items)
    
    def insert(self, item):
        """
        O(log(n))
        """
        key, data = item
        self.data[data] = key
        # heapq.heappush(self.data, item)
    
    def decrease_key(self, old_item, new_item):
        """
        O(log(n))
        """
        old_key, old_data = old_item
        new_key, new_data = new_item
        assert old_data == new_data
        self.data[old_data] = new_key
        
    def min(self):
        """
        O(1)
        """
        if self.data:
            item = self.data.peekitem()
            return (item[1], item[0])
        return None
    
    def extract_min(self):
        """
        O(log(n))
        """
        if self.data:
            item = self.data.popitem()
            return (item[1], item[0])
        return None
    
    def merge(self, other):
        """
        O(n)
        """
        self.data.update({val:key for key,val,_ in other.data.heap})
        # for key,val,_ in other.data.heap:
        #     self.insert()
        
    def has(self, key):
        return key in self.data
    
if __name__ == "__main__":
    h = BinHeapDict([(40,'c'), (50, 'd')])
    h.insert((-10,'a'))
    h.insert((-10, 'b'))
    h.decrease_key((-10,'b'),(-60,'b'))
    node = h.extract_min()
    print(h.data.heap)
    while node:
        print(node)
        node = h.extract_min()
    h = BinHeapDict()
    print(h.min())
    a = 1
    print("################")
    h1 = BinHeapDict([(40,'c'), (50, 'd')])
    h1.insert((-10,'a'))
    h1.insert((-10, 'b'))
    h1.decrease_key((-10,'b'),(-60,'b'))
    
    h2 = BinHeapDict([(-100, 'v')])
    # print(h2.min())
    h1.merge(h2)
    
    node = h1.extract_min()
    while node:
        print(node)
        node = h1.extract_min()
    print(h1.min())
    print(h2.min())
