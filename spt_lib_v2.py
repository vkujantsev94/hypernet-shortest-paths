from dis import dis
from optparse import Option
from queue import PriorityQueue
import networkx as nx
from typing import Optional, Union, Tuple, List, Set, Dict
from fib_heap import FibHeap
from bin_heap_dict import BinHeapDict
from iheap import iHeap
import util
import matplotlib.pyplot as plt
import time


MEASURE_TIME = False

def log(msg):
    with open("log.log", 'a') as fo:
        fo.write(str(msg) + "\n")

def build_spt(g: nx.DiGraph,
              source:str,
              spt_remains:Optional[nx.DiGraph] = None,
              PriorityQueue:iHeap = BinHeapDict,
              optimal:bool = True,
              min_appending_count:int = 2,
              debug:bool = False,
              logfile_path:Optional[str] = None
              ) -> nx.DiGraph:
    """
    description
    returns new SPT and non-optimal edges if they exist
    """
    if debug and logfile_path:
        open(logfile_path, 'w').write("")
        logfile = open(logfile_path, 'a')

    def debug_log(*args):
        if (not debug): return
        print(*args)
        if logfile_path:
            print(*args, logfile)

    if MEASURE_TIME:
        elapsed_backup = 0
        elapsed_initial_structs = 0
        total_elapsed_tree_checking = 0
        n_attached = 0
        total_elapsed_attaching = 0
        total_elapsed_neighbor_search = 0
        elapsed_final_building = 0
        elapsed_main = 0
        total_elaped = 0
                
        start_total = time.perf_counter()
    
    g_backup = nx.DiGraph(g)
    # init resulting spt
    initial = nx.DiGraph()
    initial.add_node(source)
    # spt = nx.DiGraph(spt_remains) or initial
    spt = spt_remains or initial
    
    if MEASURE_TIME:
        elapsed_backup = time.perf_counter() - start_total
        start_initial_structs = time.perf_counter()
    
    # init initial spt and auxiliary structures
    distances, parents = util.get_tree_info(spt, source)
    dist_heap:iHeap = PriorityQueue([(dst, node) for node,dst in distances.items()])
    joined_nodes = set(distances.keys())
    # disjoined_nodes = set(spt.nodes) - joined_nodes
    # disjoined_nodes = spt.nodes - joined_nodes
    initial_frontier = set(util.get_neighbors(g, joined_nodes))
    # initial_nodes = set(joined_nodes)
    non_optimal_edges = list()
    # debug_log('formed initial spt with nodes', joined_nodes)
    # debug_log(f"frontier: {initial_frontier}")
    
    if MEASURE_TIME:
        elapsed_initial_structs = time.perf_counter() - start_initial_structs
        start_main = time.perf_counter()

    
    # dijkstra and appending
    while dist_heap.min() is not None:
        spt_node_dst, spt_node = dist_heap.extract_min()
        if spt_node not in g_backup.nodes:
            continue
        # debug_log('got spt_node', spt_node, spt_node_dst)
        
        # we can append subtree only if
        # it is in disjoined nodes
        # and in case of optimal solution it must be in initial frontier
        need_attaching = spt_node not in joined_nodes
        if optimal:
            is_one_step_reachable = spt_node in initial_frontier and parents[spt_node] in joined_nodes and spt.has_edge(parents[spt_node], spt_node)
            need_attaching = need_attaching and is_one_step_reachable
        # if spt_node in disjoined_nodes:
        #     disjoined_nodes.remove(spt_node)
        if need_attaching:
            if debug:
                subtree = util.get_subtree(spt, spt_node)
                assert nx.algorithms.tree.recognition.is_arborescence(
                    subtree), "not an arborescence tree"
            
            if MEASURE_TIME:
                start = time.perf_counter()
            
            subtree_distances, subtree_parents = util.get_tree_info(
                spt, spt_node, source_dst=spt_node_dst, filter=joined_nodes
            )
            
            if MEASURE_TIME:
                total_elapsed_tree_checking += time.perf_counter() - start

            append_worth_it = len(subtree_distances.keys()) >= min_appending_count
            if append_worth_it:
                if MEASURE_TIME:
                    start = time.perf_counter()
                
                distances.update(subtree_distances)
                parents.update(subtree_parents)
                joined_nodes.update(subtree_distances.keys())
                # disjoined_nodes.difference_update(subtree_distances.keys())
            
                if optimal:
                    already_in_heap = set([node for node in subtree_distances.keys() if dist_heap.has(node)])
                    # if working as optimal algorithm we have to explore neighbors of appended nodes
                    #   and thus we have to push them into the heap
                    subtree_dist_heap = PriorityQueue([(dst,node) for node,dst in subtree_distances.items() if node not in already_in_heap])
                    dist_heap.merge(subtree_dist_heap)
                    # for node in nodes_to_decrease:
                    #     dist_heap.decrease_key((distances[node],node), (subtree_distances[node],node))
                else:
                    # otherwise remember non-optimal edge due to make further estimations of path optimality (outside)
                    non_optimal_edges.append((parents[spt_node], spt_node))
                # debug_log('appended nodes', len(subtree_distances.keys()))
                
                # remove nodes to avoid considering them as neighbors
                g_backup.remove_nodes_from((n for n in subtree_distances.keys() if n != spt_node))
                
                if MEASURE_TIME:
                    total_elapsed_attaching += time.perf_counter() - start
                    n_attached += len(subtree_distances.keys())
            
            # print("removed nodes", [n for n in subtree_distances.keys() if n != spt_node])
            # print('appended nodes', len(subtree_distances.keys()))
        
        if MEASURE_TIME:
            start = time.perf_counter()
        
        joined_nodes.add(spt_node)
        # dijkstra step
        for n in util.get_neighbors(g_backup, [spt_node], generator=True):
            # debug_log('\tconsidering neighbor', n)
            if n in joined_nodes:
                # debug_log(f'\tneighbor {n} is already joined')
                # debug_log(f'\t(in initial_nodes: {n in initial_nodes})')
                # debug_log(f"\t(in initial frontier: {n in initial_frontier})\n")
                continue
            old_dst = distances.get(n, float('inf'))
            new_dst = spt_node_dst + g_backup.edges[spt_node, n]['length']
            # debug_log(f'\tnew_dst {new_dst} < old_dst {old_dst}: {new_dst < old_dst}')
            if new_dst < old_dst:
                parents[n] = spt_node
                distances[n] = new_dst
                if old_dst == float('inf'):
                    dist_heap.insert((new_dst, n))
                    # debug_log(f'\tinserted')
                else:
                    dist_heap.decrease_key((old_dst, n), (new_dst, n))
                    # debug_log(f'\trefreshed')
            # debug_log()
        
        g_backup.remove_node(spt_node)
        if MEASURE_TIME:
            total_elapsed_neighbor_search += time.perf_counter() - start
    
    if MEASURE_TIME:
        elapsed_main = time.perf_counter() - start_main
        start = time.perf_counter()
    
    # dijkstra end
    # debug_log("parents:", parents)
    # debug_log("distances:", distances)
    spt = nx.DiGraph()
    spt.add_node(source)
    for node, parent in parents.items():
        spt.add_edge(parent, node, **g.edges[parent, node])
    spt.graph.update({
        "distances": distances,
        "parents": parents,
        "non_optimal_edges": non_optimal_edges
    })
    
    if MEASURE_TIME:
        elapsed_final_building = time.perf_counter() - start
        total_elaped = time.perf_counter() - start_total
    
    if MEASURE_TIME:
        times = {
        "elapsed_backup" : elapsed_backup,
        "elapsed_initial_structs" : elapsed_initial_structs,
        "total_elapsed_tree_checking" : total_elapsed_tree_checking,
        "n_attached": n_attached,
        "total_elapsed_attaching" : total_elapsed_attaching,
        "total_elapsed_neighbor_search" : total_elapsed_neighbor_search,
        "elapsed_main" : elapsed_main,
        "elapsed_final_building" : elapsed_final_building,
        "total_elaped": total_elaped
        }
        import json
        cur_measures = json.load(open(f"measure_time.json"))

        for key in cur_measures:
            cur_measures[key].append("{:.15f}".format(times[key]))
        json.dump(cur_measures, open(f"measure_time.json", "w"))
    
    return spt

# DOES IT WORK ? YES
def estimate_path_optimality(source:str, target:str, spt:nx.DiGraph, removed_edge:Tuple[str,str,float]):
    removed_edge_length = removed_edge[2]
    removed_edge = (removed_edge[0],removed_edge[1])
    
    path_length = nx.shortest_path_length(spt,source,target,weight="length")
    parents = spt.graph["parents"]
    non_optimal_edges = spt.graph["non_optimal_edges"]
    
    non_optimal_edges = set(non_optimal_edges)
    non_optimal_length = 0
    cur = target
    is_optimal = True
    last_optimal_node = None
    # print("YOY")
    while cur != source:
        edge = (parents[cur], cur)
        if edge in non_optimal_edges:
            is_optimal = False
            last_optimal_node = last_optimal_node or edge[1]
            non_optimal_length += spt.edges[parents[cur], cur]["length"]
        cur = parents[cur]
    if is_optimal:
        return (path_length,path_length)
    # print("YEY")
    upper_bound = path_length
    lower_bound = nx.shortest_path_length(spt,source,removed_edge[0],weight="length")
    lower_bound += removed_edge_length
    lower_bound += nx.shortest_path_length(spt,last_optimal_node,target,weight="length")
    return (lower_bound, upper_bound)

# def build_spt_several(g: nx.DiGraph, 
#                        sources: List[str],
#                        spts_remains: Optional[List[Optional[nx.DiGraph]]]=None,
#                        PriorityQueue: iHeap = FibHeap,
#                        is_optimal: bool = False,
#                        debug: bool = False,
#                        logfile_path: Optional[str] = None):
#     spts_remains = spts_remains or [None] * len(sources)
#     assert len(sources) == len(spts_remains)
#     num_spts = len(sources)
#     if debug and logfile_path:
#         open(logfile_path, 'w').write("")
#         logfile = open(logfile_path, 'a')

#     def debug_log(*args):
#         if (debug):
#             print(*args)
#             if logfile_path:
#                 print(*args, logfile)
    
#     # init resulting SPTs
#     spts: List[nx.DiGraph] = []
#     for i, source, spt_remains in zip(range(num_spts), sources, spts_remains):
#         initial = nx.DiGraph()
#         initial.add_node(source)
#         spt = nx.DiGraph(spt_remains) or initial
#         spts.append(spt)
        
#         # init distances, parents, dist_heap, joined and disjoined nodes for every SPT
#         distances, parents = util.get_tree_info(spt, source)
#         dist_heap = PriorityQueue([(dst,node) for node,dst in distances.items()])
#         joined_nodes = set(distances.keys())
#         spt.graph.update({
#             "dist_heap": dist_heap,
#             "parents": parents,
#             "distances": distances,
#             "joined_nodes": joined_nodes,
#             "finished": False,
#             "appended_nodes": set()
#         })
#         debug_log(f'formed {i}-th inital spt with nodes', joined_nodes)
        
#         # rebuild SPTs
#         for i,spt in enumerate(spts):
#             debug_log(f"rebuilding {i}-th spt")
#             # log(f"rebuilding {i}-th spt")
#             dist_heap:iHeap = spt.graph["dist_heap"]
#             parents:dict = spt.graph["parents"]
#             distances:dict = spt.graph["distances"]
#             joined_nodes:set = spt.graph["joined_nodes"]
#             externally_joined_nodes:set = spt.graph["externally_joined_nodes"]
        
#             #dijkstra