from gettext import find
from unittest import expectedFailure
import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
import spt_lib_v2
from pathlib import Path
import os
import pytest

log_file_path = None

def log(msg):
    with open(log_file_path, 'a') as fo:
        fo.write(str(msg) + "\n")

def test_spt_from_scratch(graph_file):
    """
    used to generate tests
    """
    g = util.load_graph(graph_file)
    s = list(g.nodes)[0]
    # spt = spt_lib.build_spt(g, s, debug=True)
    spt = spt_lib_v2.build_spt(g, s, debug=True)

    assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
    actual = nx.shortest_path_length(spt, source=s, weight='length')
    expected = nx.shortest_path_length(g, source=s, weight='length')
    assert actual == expected, "path lengths are not optimal"
    return True


def test_spt_with_prebuilt(graph_file, edge_portion):
    """
    inner function for pytests
    """
    print(f"\ninputs: {(graph_file, edge_portion)}")
    g = util.load_graph(graph_file)
    s = list(g.nodes)[0]   
    # construct valid part of spt to test building completion with prebuilt spt
    prebuilt_spt = spt_lib.build_spt(g, s)
    if len(prebuilt_spt.edges()) > 0:
        # choose edge to be removed (for constructing part of spt)
        # last edge of portion of edges will be taken
        # edge_portion is calculated randomly outside with some random seed
        edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
        edge_to_remove = list(prebuilt_spt.edges())[0 : edge_portion_count][-1]
        prebuilt_spt.remove_edges_from(list(util.get_subtree(prebuilt_spt, edge_to_remove[1]).edges()))
        prebuilt_spt.remove_edge(*edge_to_remove)
        g.remove_edge(*edge_to_remove)
        print("edge_to_remove", edge_to_remove)
    # spt = spt_lib.build_spt(g, source=s, spt_remains=prebuilt_spt, debug=True)
    spt = spt_lib_v2.build_spt(g, source=s, spt_remains=prebuilt_spt, debug=True)
    assert nx.algorithms.tree.recognition.is_arborescence(spt), f"Not an arborescence tree. Edge to remove: {edge_to_remove}."
    actual = nx.shortest_path_length(spt, source=s, weight='length')
    expected = nx.shortest_path_length(g, source=s, weight='length')
    assert actual == expected, f"Path lengths are not optimal. Edge to remove: {edge_to_remove}."
    return True

def test_spt_with_remains(graph_file, edge_portion):
    """
    inner function for pytests
    """
    print("inputs:", graph_file, edge_portion)
    g = util.load_graph(graph_file)
    
    s = list(g.nodes)[0]
    # construct valid part of spt to test building completion with prebuilt spt
    prebuilt_spt = spt_lib.build_spt(g, s)
    # print("BEFORE")
    # actual = nx.shortest_path_length(prebuilt_spt, source=s, weight='length')
    # expected = nx.shortest_path_length(g, source=s, weight='length')
    # actual_paths = nx.shortest_path(prebuilt_spt, source=s, weight='length')
    # expected_paths = nx.shortest_path(g, source=s, weight='length')
    # print("----------")
    # for node in actual:
    #     print(f"node {node}:\n\tactual:  {actual[node]} {actual_paths[node]}\n\texpected:{expected[node]} {expected_paths[node]}")
    if len(prebuilt_spt.edges()) > 0:
        # choose edge to be removed (for constructing part of spt)
        # last edge of portion of edges will be taken
        # edge_portion is calculated randomly outside with some random seed
        edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
        edge_to_remove = list(prebuilt_spt.edges())[0 : edge_portion_count][-1]
        # if edge_to_remove[0] == 0:
        #     pytest.skip(f"removing edge from source")
        print("removing edge", edge_to_remove)
        prebuilt_spt.remove_edge(*edge_to_remove)
        g.remove_edge(*edge_to_remove)
    
    # spt = spt_lib.build_spt(g, source=s, spt_remains=prebuilt_spt, debug=True)
    spt = spt_lib_v2.build_spt(g, source=s, spt_remains=prebuilt_spt, debug=True)

    assert nx.algorithms.tree.recognition.is_arborescence(spt), f"Not an arborescence tree. Edge to remove: {edge_to_remove}."
    actual = nx.shortest_path_length(spt, source=s, weight='length')
    expected = nx.shortest_path_length(g, source=s, weight='length')
    # actual_paths = nx.shortest_path(spt, source=s, weight='length')
    # expected_paths = nx.shortest_path(g, source=s, weight='length')
    # for node in actual:
    #     print(f"node {node}:\n\tactual:  {actual[node]} {actual_paths[node]}\n\texpected:{expected[node]} {expected_paths[node]}")

    assert actual == expected, f"Path lengths are not optimal. Edge to remove: {edge_to_remove}."
    return True

def test_several_spts_scratch(graph_file,num_sources,source_distance_coef):
    """
    inner function for pytests
    build SPTs for several sources
    sources are chosen ~ equally distant
    source_distance_coef - number in [0,1] 0 for closest nodes, 1 for max distant nodes
    """
    g = util.load_graph(graph_file)
    # choose random node as source
    sources = util.get_k_neighbors_around_distance(g,list(g.nodes)[0],num_sources,source_distance_coef)
    spts = spt_lib.build_several_spts(g,sources,debug=True)
    for s,spt in zip(sources,spts):
        assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
        actual = nx.shortest_path_length(spt, source=s, weight='length')
        expected = nx.shortest_path_length(g, source=s, weight='length')
        assert actual == expected, "path lengths are not optimal"
    return True
    

def test_several_spts_prebuilt(graph_file,num_sources,source_distance_coef,edge_portion):
    """
    inner function for pytests
    build SPTs for several sources
    sources are chosen ~ equally distant
    source_distance_coef - number in [0,1] 0 for closest nodes, 1 for max distant nodes
    """
    g = util.load_graph(graph_file)
    # choose random node as source
    sources = util.get_k_neighbors_around_distance(g,list(g.nodes)[0],num_sources,source_distance_coef)
    prebuilt_spts = []
    for s in sources:
        prebuilt_spt = util.build_spt_with_nx(g, s)
        if len(prebuilt_spt.edges()) > 0:
            # choose edge to be removed (for constructing part of spt)
            # last edge of portion of edges will be taken
            # edge_portion is calculated randomly outside with some random seed
            edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
            edge_to_remove = list(prebuilt_spt.edges())[0:edge_portion_count][-1]
            prebuilt_spt.remove_edges_from(list(util.get_subtree(prebuilt_spt, edge_to_remove[1]).edges()))
            prebuilt_spt.remove_edge(*edge_to_remove)
            # g.remove_edge(*edge_to_remove)
        prebuilt_spts.append(prebuilt_spt)
    spts = spt_lib.build_several_spts(g,sources,prebuilt_spts,debug=True)
    for s,spt in zip(sources,spts):
        assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
        actual = nx.shortest_path_length(spt, source=s, weight='length')
        expected = nx.shortest_path_length(g, source=s, weight='length')
        assert actual == expected, "path lengths are not optimal"
    return True
    
def test_several_spts_remains(graph_file,num_sources,source_distance_coef,edge_portion):
    """
    inner function for pytests
    build SPTs for several sources
    sources are chosen ~ equally distant
    source_distance_coef - number in [0,1] 0 for closest nodes, 1 for max distant nodes
    """
    g = util.load_graph(graph_file)
    # choose random node as source
    sources = util.get_k_neighbors_around_distance(g,list(g.nodes)[0],num_sources,source_distance_coef)
    prebuilt_spts = []
    for s in sources:
        prebuilt_spt = util.build_spt_with_nx(g, s)
        if len(prebuilt_spt.edges()) > 0:
            # choose edge to be removed (for constructing part of spt)
            # last edge of portion of edges will be taken
            # edge_portion is calculated randomly outside with some random seed
            edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
            edge_to_remove = list(prebuilt_spt.edges())[0 : edge_portion_count][-1]
            prebuilt_spt.remove_edge(*edge_to_remove)
            # g.remove_edge(*edge_to_remove)
        prebuilt_spts.append(prebuilt_spt)
    spts = spt_lib.build_several_spts(g,sources,prebuilt_spts,debug=True)
    for s,spt in zip(sources,spts):
        assert nx.algorithms.tree.recognition.is_arborescence(spt), "not an arborescence tree"
        actual = nx.shortest_path_length(spt, source=s, weight='length')
        expected = nx.shortest_path_length(g, source=s, weight='length')
        assert actual == expected, "path lengths are not optimal"
    return True




if __name__ == "__main__":
    # test_spt_clear(1000, num_nodes=100)
    # run_failed()
    
    # generate_test_resources(1000,10,"tests/resource/10")
    # generate_test_resources(100,100,"tests/resource/100")
    # generate_test_resources(50,1000,"tests/resource/1000")
    
    # with open("tests/resource/random_ints", "w") as fout:
    #     fout.write("\n".join([str(num) for num in np.random.randint(0,10000,10000)]))
    graph_file = "tests/resource/10/graph10-100"
    test_spt_with_prebuilt(graph_file, 0.5)
    pass
