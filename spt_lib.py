from queue import PriorityQueue
import networkx as nx
from typing import Optional, Union, Tuple, List, Set, Dict
from fib_heap import FibHeap
from bin_heap_dict import BinHeapDict
from iheap import iHeap
import util
import matplotlib.pyplot as plt

def log(msg):
    with open("log.log", 'a') as fo:
        fo.write(str(msg) + "\n")

def build_spt(g: nx.DiGraph,
              source: str,
              spt_remains: Optional[nx.DiGraph] = None,
              Heap: iHeap = FibHeap,
              debug: bool = False,
              logfile_path: Optional[str] = None
              ) -> nx.DiGraph:
    """
    Dijkstra's algorithm (with fib heap) for building shortest path tree (spt)

    Parameters
    ----------
    g: primary graph in which shortest paths are calculated
    source: source node, the root of spt
    remains: (optional) remains of previous shortest path tree that will be used to shorten the calculations (may be disjoined). 
        The nodes reachable from source will be immediatly added to the spt
    Heap: the container that implements priority queue abstraction (the IHeap interface)
    debug: (optional) debug parameter, enables logs and additionals checks of correctness
    logfile_path: (optional) debug parameter, if specified, logs will also be printed to the log file
    """
    if debug and logfile_path:
        open(logfile_path, 'w').write("")
        logfile = open(logfile_path, 'a')

    def debug_log(*args):
        if (debug):
            print(*args)
            if logfile_path:
                print(*args, logfile)

    # debug_log("draw g with removed edge")
    # nx.draw_networkx(g, pos=util.get_pos(g))
    # plt.show()

    # init resulting spt
    initial = nx.DiGraph()
    initial.add_node(source)
    spt = nx.DiGraph(spt_remains) or initial
    
    # nx.draw_networkx(spt, pos=util.get_pos(g))
    # plt.show()

    # init auxiliary structures
    distances, parents = util.get_tree_info(spt, source)
    dist_heap:iHeap = Heap([(dst, node) for node,dst in distances.items()])
    spt_joined_nodes = set(distances.keys())
    spt_disjoined_nodes = set(spt.nodes) - spt_joined_nodes

    debug_log('formed spt with nodes', spt_joined_nodes)

    adjusted_nodes = set()

    # dijkstra
    while dist_heap.min() is not None:
        spt_node_dst, spt_node = dist_heap.extract_min()
        # debug_log(f"[distance to 3:{distances.get(3,float('inf'))}]")
        
        debug_log('got spt_node', spt_node, spt_node_dst)
        if spt_node in spt_disjoined_nodes:
            spt_disjoined_nodes.remove(spt_node)
            debug_log('spt_node is in disjoined nodes, adjusting its subtree')
            if debug:
                subtree = util.get_subtree(spt, spt_node)
                assert nx.algorithms.tree.recognition.is_arborescence(
                    subtree), "not an arborescence tree"
            subtree_distances, subtree_parents = util.get_tree_info(
                spt, spt_node, source_dst=spt_node_dst, filter=spt_joined_nodes
            )
            # subtree_distances = {node:dst for (node,dst) in subtree_distances.items() if dst > distances.get(node, float('inf'))}
            # subtree_parents = {node:subtree_parents[node] for node in subtree_distances}

            distances.update(subtree_distances)
            parents.update(subtree_parents)
            # subtree_dist_heap = Heap([(dst,node) for node,dst in subtree_distances.items()])
            # dist_heap.merge(subtree_dist_heap)
            
            spt_joined_nodes.update(subtree_distances.keys())
            spt_disjoined_nodes.difference_update(subtree_distances.keys())
            # adjusted_nodes.update(subtree_distances.keys())
            
            debug_log('adjusted nodes', subtree_distances.keys())
        spt_joined_nodes.add(spt_node)
        
        
        for n in util.get_neighbors(g, [spt_node], generator=True):
            debug_log('\tconsidering neighbor', n)
            if n in spt_joined_nodes:
                debug_log(f'\tneighbor {n} is already joined\n')
                continue
            old_dst = distances.get(n, float('inf'))
            new_dst = spt_node_dst + g.edges[spt_node, n]['length']
            debug_log(f'\tnew_dst {new_dst} < old_dst {old_dst}: {new_dst < old_dst}')
            if new_dst < old_dst:
                parents[n] = spt_node
                distances[n] = new_dst
                if old_dst == float('inf'):
                    dist_heap.insert((new_dst, n))
                    debug_log(f'\tinserted')
                else:
                    dist_heap.decrease_key((old_dst, n), (new_dst, n))
                    debug_log(f'\trefreshed')
            debug_log()
    debug_log("parents:", parents)
    debug_log("distances:", distances)
    if debug:
        failed_nodes = []
        for node in spt.nodes():
            if node in spt_disjoined_nodes and node in parents:
                failed_nodes.append((node, parents[node]))
        assert len(failed_nodes) == 0, f"Nodes' parents are corrupted: {failed_nodes}"

    # remains of old spt could contain branches that are not visited during the search and thus they are unreachable
    # those nodes must be pruned
    # this is impossible in case of single spt
    # but in case of rebuilding multiple spt-s this case may appear
    # nx.draw_networkx(spt, pos=util.get_pos(g))
    # plt.show()

    debug_log("start prunning from nodes:", spt_disjoined_nodes)
    pruned_nodes = []
    nodes_to_check = spt_disjoined_nodes
    while len(nodes_to_check) > 0:
        node = nodes_to_check.pop()
        if node in spt_joined_nodes:
            continue
        pruned_nodes.append(node)
        nodes_to_check.update(util.get_neighbors(spt, [node], generator=True))
    if debug:
        assert len(spt_disjoined_nodes) == 0, spt_disjoined_nodes
    spt.remove_nodes_from(pruned_nodes)
    debug_log("pruned nodes:", pruned_nodes)
    spt = nx.DiGraph()
    spt.add_node(source)
    for node, parent in parents.items():
        spt.add_edge(parent, node, **g.edges[parent, node])
    return spt


#### descr ####
# !!log every step of algotirhm for easy debugging
#   seems like there may appear cases like: /\
#                                           \/
#   they should NOT be
#   if they are, there is a problem in mathematical suppose of optimality of added paths
#   there may be some roots of subtrees that must be pruned, but by our assumption they deliver optimal paths to the graph
# 
# following the idea of the algorithm, we have to explore leaves of added nodes
# this is achieved automatically since we add ALL the tree nodes (including spt remains) to the dijkstra's heap
#   
# all trees has precalculated joined_spt and disjoined_spt nodes
# 
#### algorithm ####
# rebuild every spt one by one
# when rebuilding some tree
#   if node is disjoined_spt in some other tree A
#       and if its child is in A's disjoined_spt then:
#           add the whole subtree to A's spt
#           remember this node as added subtree root
#   else:
#           skip it
# 
# after all spts are rebuilt
#   try prune all roots of added subtrees
# 
def build_several_spts(g: nx.DiGraph, 
                       sources: List[str],
                       spts_remains: Optional[List[Optional[nx.DiGraph]]]=None,
                       Heap: iHeap = FibHeap,
                       debug: bool = False,
                       logfile_path: Optional[str] = None):
    spts_remains = spts_remains or [None] * len(sources)
    assert len(sources) == len(spts_remains)
    num_spts = len(sources)
    if debug and logfile_path:
        open(logfile_path, 'w').write("")
        logfile = open(logfile_path, 'a')

    def debug_log(*args):
        if (debug):
            print(*args)
            if logfile_path:
                print(*args, logfile)
    
    # init resulting SPTs
    spts: List[nx.DiGraph] = []
    for i, source, spt_remains in zip(range(num_spts), sources, spts_remains):
        initial = nx.DiGraph()
        initial.add_node(source)
        spt = nx.DiGraph(spt_remains) or initial
        spts.append(spt)
        
        # init distances, parents, dist_heap, joined and disjoined nodes for every SPT
        distances, parents = util.get_tree_info(spt, source)
        dist_heap = Heap([(dst,node) for node,dst in distances.items()])
        joined_nodes = set(distances.keys())
        disjoined_nodes = set(spt.nodes) - joined_nodes 
        # if ((1.4156911613973313, 1) in [(dst,node) for node,dst in distances.items()]):
        #     print("BIBA", 1 in joined_nodes)
        spt.graph.update({
            "dist_heap": dist_heap,
            "parents": parents,
            "distances": distances,
            "joined_nodes": joined_nodes,
            "disjoined_nodes": disjoined_nodes,
            "props": dict(),
            "externally_joined_nodes": set()
        })
        debug_log(f'formed {i}-th inital spt with nodes', joined_nodes)
        # nx.draw_networkx(spt, pos=util.get_pos(g))
        # plt.show()
    # rebuild SPTs
    for i,spt in enumerate(spts):
        debug_log(f"rebuilding {i}-th spt")
        # log(f"rebuilding {i}-th spt")
        dist_heap:iHeap = spt.graph["dist_heap"]
        parents:dict = spt.graph["parents"]
        distances:dict = spt.graph["distances"]
        joined_nodes:set = spt.graph["joined_nodes"]
        disjoined_nodes:set = spt.graph["disjoined_nodes"]
        externally_joined_nodes:set = spt.graph["externally_joined_nodes"]
        #dijkstra
        while dist_heap.min() is not None:
            spt_node_dst, spt_node = dist_heap.extract_min()
            
            debug_log('got spt_node', spt_node)
            if spt_node in disjoined_nodes:
                disjoined_nodes.remove(spt_node)
                debug_log('spt_node is in disjoined nodes, adjusting its subtree')
                if debug:
                    subtree = util.get_subtree(spt, spt_node)
                    debug_log(f"will try to adjust {subtree}")
                    assert \
                        nx.algorithms.tree.recognition.is_arborescence(subtree), \
                        "not an arborescence tree"
                # update distances and parents directly to shorten calculations
                subtree_distances, subtree_parents = util.get_tree_info(
                    spt, spt_node, source_dst=spt_node_dst, filter=joined_nodes)
                
                distances.update(subtree_distances)
                parents.update(subtree_parents)
                # shorten calculations using merge implementation of heap
                subtree_dist_heap = Heap([(dst,node) for node,dst in subtree_distances.items()])
                dist_heap.merge(subtree_dist_heap)
                
                joined_nodes.update(subtree_distances.keys())
                disjoined_nodes.difference_update(subtree_distances.keys())               
                
                debug_log('adjusted nodes', subtree_parents.keys())
                # try adjust subtree to other SPTs
                # TODO maybe move it to general cycle
                for another_i,another_spt in enumerate(spts):
                    if another_spt.graph["props"].get("finished") is True:
                        continue
                    if spt is another_spt:
                        continue
                    # THIN PLACE, THIN PLACE
                    # if spt_node is not joined to another_spt yet, we can not calc distances to its subtrees
                    #    (because) we have not found spt_nodes actual ditance in another_spt
                    #    so, we have to skip it
                    # possibly it (and its children) could be remembered due to calculate their distances fast when meet them
                    #    still, it would take O(n) - same as simple joining
                    #
                    # if spt_node is not joined to another_spt, we can't mark it as joined due to Dijkstra's invariant
                    #    and thus, it can't be added to dist_heap, and etc
                    if spt_node not in another_spt.graph["joined_nodes"]:
                        continue
                    another_dist_heap:iHeap = another_spt.graph["dist_heap"]
                    another_parents:dict = another_spt.graph["parents"]
                    another_distances:dict = another_spt.graph["distances"]
                    another_joined_nodes:set = another_spt.graph["joined_nodes"]
                    another_disjoined_nodes:set = another_spt.graph["disjoined_nodes"]
                    for ssn in util.get_neighbors(spt, [spt_node], generator=True):
                        # if there is edge (spt_node,ssn)
                        if ssn in another_spt[spt_node]:
                            # print(f"!!!!!!{spt_node,ssn}\n!!!!!!{spt.edges()}\n!!!!!!{another_spt.edges()}")
                            # add ssn subtree to another_spt
                            ssn_dst = another_distances[spt_node] + another_spt.edges[spt_node,ssn]['length']
                            ssn_dsts, ssn_pars = util.get_tree_info(spt, ssn, source_dst=ssn_dst) #, filter=another_joined_nodes
                            ssn_dist_heap = Heap([(dst,node) for node,dst in ssn_dsts.items() if node not in another_joined_nodes])
                            debug_log(f"merging nodes {ssn_pars} to {another_i}")
                            
                            another_dist_heap.merge(ssn_dist_heap)
                            another_distances.update(ssn_dsts)
                            another_parents.update(ssn_pars)
                            another_parents[ssn] = spt_node
                            another_joined_nodes.update(ssn_pars.keys())
                            another_disjoined_nodes.difference_update(ssn_pars.keys())
            joined_nodes.add(spt_node)
            # dijkstra step
            for n in util.get_neighbors(g, [spt_node], generator=True):
                debug_log('\tconsidering neighbor', n)
                if n in joined_nodes:
                    debug_log(f'\tneighbor {n} is already joined\n')
                    continue
                old_dst = distances.get(n, float('inf'))
                new_dst = spt_node_dst + g.edges[spt_node, n]['length']
                debug_log(f'\tnew_dst {new_dst} < old_dst {old_dst}: {new_dst < old_dst}')
                if new_dst < old_dst:
                    parents[n] = spt_node
                    distances[n] = new_dst
                    if old_dst == float('inf'):
                        dist_heap.insert((new_dst, n))
                        debug_log(f'\tinserted')
                    else:
                        dist_heap.decrease_key((old_dst, n), (new_dst, n))
                        debug_log(f'\trefreshed')
                debug_log()
        debug_log(f"{i}-th parents:", parents)
        debug_log(f"{i}-th distances:", distances)
        
        # remains of old spt could contain branches that are not visited during the search and thus they are unreachable
        # those nodes must be pruned
        # this is impossible in case of single spt
        # but in case of rebuilding multiple spt-s this case may appear
        debug_log("start prunning from nodes:", disjoined_nodes)
        pruned_nodes = []
        nodes_to_check = disjoined_nodes
        while len(nodes_to_check) > 0:
            node = nodes_to_check.pop()
            if node in joined_nodes:
                continue
            pruned_nodes.append(node)
            nodes_to_check.update(util.get_neighbors(spt, [node], generator=True))
        spt.remove_nodes_from(pruned_nodes)
        debug_log("pruned nodes:", pruned_nodes)
        
        if debug:
            failed_nodes = []
            for node in spt.nodes():
                if node in disjoined_nodes and node in parents:
                    failed_nodes.append((node, parents[node]))
            assert len(failed_nodes) == 0, f"Nodes' parents are corrupted: {failed_nodes}"
        
        for node, parent in parents.items():
            spt.add_edge(parent, node, **g.edges[parent, node])
        # nx.draw_networkx(spt, pos=util.get_pos(g))
        # plt.show()
        if debug:
            assert \
                nx.algorithms.tree.recognition.is_arborescence(spt), \
                "not an arborescence tree"
            print("OK")
        spt.graph["props"]["finished"] = True
    return spts

def dijkstra_step(g,dist_heap,distances,parents,joined_nodes,spt_node_dst,spt_node):
    # dijkstra step
    for n in util.get_neighbors(g, [spt_node], generator=True):
        # debug_log('\tconsidering neighbor', n)
        if n in joined_nodes:
            # debug_log(f'\tneighbor {n} is already joined\n')
            continue
        old_dst = distances.get(n, float('inf'))
        new_dst = spt_node_dst + g.edges[spt_node, n]['length']
        # debug_log(f'\tnew_dst {new_dst} < old_dst {old_dst}: {new_dst < old_dst}')
        if new_dst < old_dst:
            parents[n] = spt_node
            distances[n] = new_dst
            if old_dst == float('inf'):
                dist_heap.insert((new_dst, n))
                # debug_log(f'\tinserted')
            else:
                dist_heap.decrease_key((old_dst, n), (new_dst, n))
                # debug_log(f'\trefreshed')
        # debug_log()
    return spt_node_dst, spt_node

def build_several_spts_new(g: nx.DiGraph, 
                       sources: List[str],
                       spts_remains: Optional[List[Optional[nx.DiGraph]]]=None,
                       Heap: iHeap = FibHeap,
                       debug: bool = False,
                       logfile_path: Optional[str] = None):
    spts_remains = spts_remains or [None] * len(sources)
    assert len(sources) == len(spts_remains)
    num_spts = len(sources)
    if debug and logfile_path:
        open(logfile_path, 'w').write("")
        logfile = open(logfile_path, 'a')

    def debug_log(*args):
        if (debug):
            print(*args)
            if logfile_path:
                print(*args, logfile)
    
    # init data for building SPTs
    spts: List[nx.DiGraph] = []
    for i, source, spt_remains in zip(range(num_spts), sources, spts_remains):
        initial = nx.DiGraph()
        initial.add_node(source)
        spt = nx.DiGraph(spt_remains) or initial
        spts.append(spt)

        # init distances, parents, dist_heap, joined and disjoined nodes for every SPT
        distances, parents = util.get_tree_info(spt, source)
        dist_heap = Heap([(dst,node) for node,dst in distances.items()])
        joined_nodes = set(distances.keys())
        disjoined_nodes = set(spt.nodes) - joined_nodes 
        spt.graph.update({
            "dist_heap": dist_heap,
            "parents": parents,
            "distances": distances,
            "joined_nodes": joined_nodes,
            "disjoined_nodes": disjoined_nodes,
            "props": {"finished": False}
        })
        debug_log(f'formed {i}-th inital spt with nodes', joined_nodes)
    while True:
        all_done = True
        for i, spt in enumerate(spts):
            done = spt.graph["props"]["finished"] = spt.graph["dist_heap"].min() is None
            all_done = all_done and done
        if all_done: break
        
        not_finished_spts = [spt for spt in spts if spt.graph["props"]["finished"] is False]
        for i, spt in enumerate(not_finished_spts):
            dist_heap:iHeap = spt.graph["dist_heap"]
            parents:dict = spt.graph["parents"]
            distances:dict = spt.graph["distances"]
            joined_nodes:set = spt.graph["joined_nodes"]
            disjoined_nodes:set = spt.graph["disjoined_nodes"]
            
            spt_node_dst, spt_node = dist_heap.extract_min()
            if spt_node in disjoined_nodes:
                log("---")
                log(f"going to adjust edges: {util.get_subtree(spt, spt_node).edges()}")
                adjust_candidates = []
                for nf_spt in not_finished_spts:
                    if spt is nf_spt: continue
                    if spt_node not in nf_spt.graph["joined_nodes"]: continue
                    adjust_candidates.append(nf_spt)
                subtree_distances,subtree_parents,infos = util.get_tree_info_several(spt, spt_node, adjust_candidates)
                
                distances.update(subtree_distances)
                parents.update(subtree_parents)
                subtree_dist_heap = Heap([(dst,node) for node,dst in subtree_distances.items()]) # if node not in joined_nodes
                dist_heap.merge(subtree_dist_heap)      
                joined_nodes.update(subtree_distances.keys())
                disjoined_nodes.difference_update(subtree_distances.keys())
                
                
                log(f"merging nodes {subtree_distances.keys()} to graph")
                merging_info = [info[0].keys() for info in infos]
                log(f"other spts: {merging_info}")
                log(f"spt subtree edges: {util.get_subtree(spt, spt_node).edges()}")
                # log(f"g subtree edges: {util.get_subtree(g, spt_node).edges()}")
                
                for nf_i,(nf_distances,nf_parents) in enumerate(infos):
                    nf_spt = not_finished_spts[nf_i]
                    nf_spt.graph["distances"].update(nf_distances)
                    nf_spt.graph["parents"].update(nf_parents)
                    nf_sub_heap = Heap([(dst,node) for node,dst in nf_distances.items()]) #  if node not in nf_spt.graph["joined_nodes"]
                    nf_spt.graph["dist_heap"].merge(nf_sub_heap)
                    nf_spt.graph["joined_nodes"].update(nf_distances.keys())
                    nf_spt.graph["disjoined_nodes"].difference_update(nf_distances.keys())

            # make dijkstra step
            dijkstra_step(g,dist_heap,distances,parents,joined_nodes,spt_node_dst,spt_node)
                
            