from distutils.command.build_scripts import first_line_re
from itertools import count
import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
import spt_lib_v2
from pathlib import Path
import os
from collections import defaultdict

def log(msg):
    with open("log.log", 'a') as fo:
        fo.write(str(msg) + "\n")


def test_spt_vs_nx_from_scratch(graph_files):
    spt_times = []
    nx_times = []

    for graph_file in graph_files:
        g = util.load_graph(graph_file)
        s = list(g.nodes)[0]
        start = time.time()
        # spt = spt_lib.build_spt(g, s)
        spt_lib_v2.build_spt(g, s)
        spt_time = time.time() - start

        start = time.time()
        nx_spt = nx.shortest_path(g, source=s, weight='length')
        nx_time = time.time() - start

        spt_times.append(spt_time)
        nx_times.append(nx_time)
    spt_mean_time = np.array(spt_times).mean()
    nx_mean_time = np.array(nx_times).mean()
    msg = f"spt_mean_time={spt_mean_time}, nx_mean_time={nx_mean_time}, diff={nx_mean_time - spt_mean_time}"
    log(msg)
    assert spt_mean_time < nx_mean_time, msg
    return True

def test_spt_vs_nx_with_prebuilt(graph_files, edge_portions):
    spt_times = []
    nx_times = []
    for graph_file, edge_portion in zip(graph_files, edge_portions):
        g = util.load_graph(graph_file)
        s = list(g.nodes)[0]
        
        # construct valid part of spt to test building completion with prebuilt spt
        prebuilt_spt = spt_lib.build_spt(g, s)
        if len(prebuilt_spt.edges()) > 0:
            # choose edge to be removed (for constructing part of spt)
            # last edge of portion of edges will be taken
            # edge_portion is calculated randomly outside with some random seed
            edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
            edge_to_remove = list(prebuilt_spt.edges())[0 : edge_portion_count][-1]
            prebuilt_spt.remove_edges_from(list(util.get_subtree(prebuilt_spt, edge_to_remove[1]).edges()))
            prebuilt_spt.remove_edge(*edge_to_remove)
        start = time.time()
        # spt = spt_lib.build_spt(g, source=s, spt_remains=prebuilt_spt)
        spt_lib_v2.build_spt(g, source=s, spt_remains=prebuilt_spt)
        spt_time = time.time() - start
        start = time.time()
        nx_graph = nx.shortest_path_length(g, source=s, weight='length')
        nx_time = time.time() - start
        
        spt_times.append(spt_time)
        nx_times.append(nx_time)
    spt_mean_time = np.array(spt_times).mean()
    nx_mean_time = np.array(nx_times).mean()
    msg = f"spt_mean_time={spt_mean_time}, nx_mean_time={nx_mean_time}, diff={nx_mean_time - spt_mean_time}"
    log(msg)
    assert spt_mean_time < nx_mean_time, msg
    return True

def test_spt_vs_nx_with_remains(graph_files, edge_portions):
    spt_times = []
    nx_times = []
    for graph_file, edge_portion in zip(graph_files, edge_portions):
        g = util.load_graph(graph_file)
        s = list(g.nodes)[0]
        
        # construct valid part of spt to test building completion with prebuilt spt
        prebuilt_spt = spt_lib.build_spt(g, s)
        if len(prebuilt_spt.edges()) > 0:
            # choose edge to be removed (for constructing part of spt)
            # last edge of portion of edges will be taken
            # edge_portion is calculated randomly outside with some random seed
            edge_portion_count = int(len(prebuilt_spt.edges()) * edge_portion) + 1
            edge_to_remove = list(prebuilt_spt.edges())[0 : edge_portion_count][-1]
            prebuilt_spt.remove_edges_from(list(util.get_subtree(prebuilt_spt, edge_to_remove[1]).edges()))
            prebuilt_spt.remove_edge(*edge_to_remove)
        start = time.time()
        # spt = spt_lib.build_spt(g, source=s, spt_remains=prebuilt_spt)
        spt_lib_v2.build_spt(g, source=s, spt_remains=prebuilt_spt)
        spt_time = time.time() - start
        start = time.time()
        nx_graph = nx.shortest_path_length(g, source=s, weight='length')
        nx_time = time.time() - start
        
        spt_times.append(spt_time)
        nx_times.append(nx_time)
    spt_mean_time = np.array(spt_times).mean()
    nx_mean_time = np.array(nx_times).mean()
    msg = f"spt_mean_time={spt_mean_time}, nx_mean_time={nx_mean_time}, diff={nx_mean_time - spt_mean_time}"
    log(msg)
    assert spt_mean_time < nx_mean_time, msg
    return True

def test_several_paths(graph_files, source_distance_coefs, num_sources, edge_portion):
    # g = util.load_graph(graph_file)
    # source_distance_coefs = [0,1]
    times = defaultdict(list)
    for gf in graph_files:
        g = util.load_graph(gf)
        log(f"{g}")
        for sdc in source_distance_coefs:
            log("=========")
            log(f"sdc {sdc}")
            
            first_source = list(g.nodes)[0]
            sources = util.get_k_neighbors_around_distance(g,first_source,num_sources,sdc)
            log(f"sources: {sources}")
            log("")
            
            edge_to_remove = None
            if sdc == 0:
                util.force_set_edge(g,0,1,length=0.00001)
                util.force_set_edge(g,4,1,length=0.00001)
                util.force_set_edge(g,1,672,length=0.00001)
                edge_to_remove = (1,672)
            
            prebuilt_spts = [util.build_spt_with_nx(g, s) for s in sources]
            def count_edge(l,e):
                counter = 0
                for el in l:
                    if el == e:
                        counter += 1
                return counter
            all_edges = []
            for pbs in prebuilt_spts:
                all_edges.extend(pbs.edges())
            
            repeated_common_edges = [e for e in all_edges if count_edge(all_edges,e) == len(prebuilt_spts)]
            unique_edges = set()
            common_edges = []
            for e in repeated_common_edges:
                if e not in unique_edges:
                    common_edges.append(e)
                unique_edges.add(e)
            
            edge_portion_count = int(len(common_edges) * edge_portion) + 1
            edge_to_remove = edge_to_remove or common_edges[0 : edge_portion_count][-1]            
            log(f"edge_to_remove {edge_to_remove}")
            
            for pbs in prebuilt_spts:
                # nx.draw_networkx(pbs, pos=util.get_pos(g))
                # plt.show()
                pbs.remove_edge(*edge_to_remove)
            start = time.time()
            spts = spt_lib.build_several_spts_new(g,sources, spts_remains=prebuilt_spts)
            end = time.time()
            times[sdc].append(end - start)
    mean_times = {sdc: np.array(times[sdc]).mean() for sdc in source_distance_coefs}
    log(mean_times)
    for i in range(1,len(source_distance_coefs)):
        sdc_prev,sdc_cur = source_distance_coefs[i-1], source_distance_coefs[i]
        assert mean_times[sdc_cur] > mean_times[sdc_prev]
    return True
