from iheap import iHeap
import fibheap as fh
from typing import Optional,Iterable

class FibHeap(iHeap):
    """
    Wrapper for fibheap lib.
    """
    def __init__(self, items:Optional[Iterable]=None):
        """
        O(n)
        """
        self.h = fh.makefheap()
        self.nodes = dict()
        self.containing_keys = set()
        if items is not None:
            for item in items:
                try:
                    self.insert(item)
                except Exception as e:
                    raise Exception(f"Failed insert with {item}")

    def insert(self, item):
        """
        O(1)
        """
        # nodes_len = len(self.nodes.keys())
        # heap_nodes_len = self.h.num_nodes
        # assert nodes_len == heap_nodes_len, (nodes_len, heap_nodes_len)
        assert item not in self.nodes
        node = fh.Node(item)
        self.nodes[item] = node
        self.containing_keys.add(item[1])
        self.h.insert(node)

    def decrease_key(self, old_item, new_item):
        """
        O(1)
        """
        node = self.nodes.get(old_item)
        if node is None:
            raise Exception(f"Item '{old_item}' does not exist in heap")
        self.h.decrease_key(node, new_item)
        del self.nodes[old_item]
        self.nodes[new_item] = node

    def min(self):
        """
        O(1)
        """
        if self.h.min is not None:
            return self.h.min.key
        return None

    def extract_min(self):
        """
        O(log(n))
        """
        nodes_len = len(self.nodes.keys())
        heap_nodes_len = self.h.num_nodes
        assert nodes_len == heap_nodes_len, (nodes_len, heap_nodes_len)
        
        extracted_node = self.h.extract_min()
        if extracted_node is not None:
            # if extracted_node.key not in self.nodes:
            #     print("!",extracted_node,self.nodes, self.h.min)
            del self.nodes[extracted_node.key]
            # print(self.containing_keys)
            self.containing_keys.remove(extracted_node.key[1])
            return extracted_node.key
        return None
    
    def merge(self, other):
        """
        O(1)
        """
        nodes_len = len(self.nodes.keys())
        heap_nodes_len = self.h.num_nodes
        assert nodes_len == heap_nodes_len, (nodes_len, heap_nodes_len)
        # print([i for i in self.nodes.keys() if i[1]==70])
        
        self.h.union(other.h)
        self.nodes.update(other.nodes)
        self.containing_keys.update(other.containing_keys)
        
        nodes_len = len(self.nodes.keys())
        heap_nodes_len = self.h.num_nodes
        
        if (len(self.nodes.keys()) != self.h.num_nodes):
            ns = []
            while self.h.min is not None:
                ns.append(self.h.extract_min().key)
            print([i for i in self.nodes.keys() if i[1]==70])
            print([i for i in ns if i[1]==70])
            print("ns", ns)
            print("nn", self.nodes.keys())
        assert nodes_len == heap_nodes_len, (nodes_len, heap_nodes_len)

    def keys(self):
        return self.nodes.keys()

    def has(self, key):
        return key in self.containing_keys

if __name__ == "__main__":
    h = FibHeap([(40,'c'), (50, 'd')])
    h.insert((-10,'a'))
    h.insert((-10, 'b'))
    h.decrease_key((-10,'b'),(-60,'b'))
    node = h.extract_min()
    print(h.data.heap)
    while node:
        print(node)
        node = h.extract_min()
    h = FibHeap()
    print(h.min())
    a = 1
    print("################")
    h1 = FibHeap([(40,'c'), (50, 'd')])
    h1.insert((-10,'a'))
    h1.insert((-10, 'b'))
    h1.decrease_key((-10,'b'),(-60,'b'))
    
    h2 = FibHeap([(-100, 'v')])
    # print(h2.min())
    h1.merge(h2)
    
    node = h1.extract_min()
    while node:
        print(node)
        node = h1.extract_min()
    print(h1.min())
    print(h2.min())
    a = 1