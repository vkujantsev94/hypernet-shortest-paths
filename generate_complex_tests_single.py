from distutils.command.build_scripts import first_line_re
from itertools import count
import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
import spt_lib_v2
from pathlib import Path
import os
from collections import defaultdict
from gettext import find
from unittest import expectedFailure
import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
import spt_lib_v2
from pathlib import Path
import os
import pytest
import random
import pandas as pd
import functools

def my_timeit(n_tests, func, *args,**kwargs):
    r = float('inf')
    for i in range(n_tests):
        start = time.perf_counter()
        func(*args,**kwargs)
        end = time.perf_counter()
        elapsed = end - start
        r = min(r,elapsed)
    return r
    
# def measure_time(f):
#     functools.wraps(f)
#     def inner():
        

def measure_time_atomic(g:nx.Graph,source,edge_dist_coef,n_tests=5):
    g = nx.Graph(g)
    result = {"node":source,"e_dist":edge_dist_coef,"t_scratch":None,"t_prebuilt":None,"t_remains":None,"success":None}
    old_spt = util.build_spt_with_nx(g,source)
    
    possible_edges_to_remove = util.get_edges_on_distance(old_spt,source,edge_dist_coef)
    
    # edge_to_remove = possible_edges_to_remove[np.random.randint(len(possible_edges_to_remove))]
    edge_to_remove = max(possible_edges_to_remove, key=lambda e:util.get_num_children(old_spt,e[1]))
    
    # print("EDGE TO REMOVE", edge_to_remove)
    g.remove_edge(*edge_to_remove)
    old_spt.remove_edge(*edge_to_remove)
    
    # print("===REMAINS===")
    remains = old_spt
    t_remains = my_timeit(n_tests,spt_lib_v2.build_spt,g,source,spt_remains=remains,optimal=True)
    result["t_remains"] = t_remains
    
    # print("===PREBUILT===")
    old_spt.remove_edges_from(list(util.get_subtree(old_spt, edge_to_remove[1]).edges()))
    prebuilt_spt = old_spt
    t_prebuilt = my_timeit(n_tests,spt_lib_v2.build_spt,g,source,spt_remains=prebuilt_spt,optimal=True)
    result["t_prebuilt"] = t_prebuilt
    
    # print("===SCRATCH===")
    t_scratch = my_timeit(n_tests,spt_lib_v2.build_spt,g,source,optimal=True)
    result["t_scratch"] = t_scratch
    
    result["success"] = t_scratch >= t_prebuilt >= t_remains
    return result

def measure_time_graph(graph_file_path, num_sources, num_edge_dist_coefs, random_seed=None):
    # print("GRAPH FILE PATH", graph_file_path)
    g = util.load_graph(graph_file_path)
    if random_seed is not None:
        np.random.seed(random_seed)
        random.seed(random_seed)
    results = {"file":[],"node":[],"e_dist":[],"t_scratch":[],"t_prebuilt":[],"t_remains":[],"success":[]}
    
    nodes = g.nodes.keys()
    corner_nodes = [node for node in nodes if len(g[node].keys()) in [3,4]]
    
    random_sources = [node for node in random.sample(nodes,num_sources)]
    # random_sources = [node for node in random.sample(corner_nodes,num_sources)]
    
    random_edge_dists = list(np.random.rand(num_edge_dist_coefs) / 1)
    for source in random_sources:
        for edge_dist_coef in random_edge_dists:
            # print('SOURCE', source)
            # print('EDGE DIST COEF', edge_dist_coef)
            atomic_result = measure_time_atomic(g,source,edge_dist_coef)
            results["file"].append(graph_file_path)
            results["node"].append(source)
            results["e_dist"].append(edge_dist_coef)
            results["t_scratch"].append(atomic_result["t_scratch"])
            results["t_prebuilt"].append(atomic_result["t_prebuilt"])
            results["t_remains"].append(atomic_result["t_remains"])
            results["success"].append(atomic_result["success"])
    return results

def measure_distance_error_atomic(g: nx.DiGraph, source, edge_dist_coef):
    # optimal - lower_bound
    g = nx.Graph(g)
    results = {"source":[],"target":[],"is_initial":[],"e_dist":[],"lower_bound":[],"upper_bound":[],"approx_length":[],"optimal_length":[],"percent":[],"error":[],"success":[],"o-lb":[]}
    old_spt = util.build_spt_with_nx(g,source)
    # old_optimal_paths = nx.shortest_path(old_spt, source, weight="length")
    # old_optimal_lengths = nx.shortest_path_length(old_spt, source, weight="length")
    
    possible_edges_to_remove = util.get_edges_on_distance(old_spt,source,edge_dist_coef)
    
    # edge_to_remove = possible_edges_to_remove[np.random.randint(len(possible_edges_to_remove))]
    edge_to_remove = max(possible_edges_to_remove, key=lambda e:util.get_num_children(old_spt,e[1]))
    edge_to_remove_length = g.edges[edge_to_remove]["length"]
    
    # print("EDGE TO REMOVE", edge_to_remove)
    g.remove_edge(*edge_to_remove)
    old_spt.remove_edge(*edge_to_remove)
    
    remains = old_spt
    distances, _ = util.get_tree_info(old_spt, source)
    initial_nodes = set(distances.keys())
    assert len(initial_nodes) < len(g.nodes())
    
    spt = spt_lib_v2.build_spt(g,source,spt_remains=remains,optimal=False)
    # print("SPT")
    
    optimal_spt = util.build_spt_with_nx(g,source)
    optimal_lengths = nx.shortest_path_length(optimal_spt,source,weight="length")
    # print("OPTIMAL SPT")
    target_nodes = g.nodes() - set([source])
    for target in target_nodes:
        if target in initial_nodes:
            continue
        lower_bound, upper_bound = spt_lib_v2.estimate_path_optimality(source,target,spt,(*edge_to_remove,edge_to_remove_length))
        # print("ESTIMATE")
        approx_length = upper_bound
        optimal_length = optimal_lengths[target]
        error = (approx_length - optimal_length) / approx_length
        
        success = lower_bound <= optimal_length <= upper_bound or np.isclose(lower_bound, optimal_length)# and np.isclose(optimal_length - upper_bound, 0)
        # 
        results["source"].append(source)
        results["target"].append(target)
        results["is_initial"].append(target in initial_nodes)
        results["e_dist"].append(edge_dist_coef)
        results["lower_bound"].append(lower_bound)
        results["upper_bound"].append(upper_bound)
        results["approx_length"].append(approx_length)
        results["optimal_length"].append(optimal_length)

        percent = 1 if np.isclose(upper_bound, lower_bound) else (optimal_length - lower_bound) / (upper_bound - lower_bound)
        results["percent"].append(percent)
        results["error"].append(error)
        results["success"].append(success)
        results["o-lb"].append(abs(optimal_length - lower_bound))
    return results

def measure_distance_error_graph(graph_file_path, num_sources, num_edge_dist_coefs, random_seed=None):
    # print("GRAPH FILE PATH", graph_file_path)
    g = util.load_graph(graph_file_path)
    if random_seed is not None:
        np.random.seed(random_seed)
        random.seed(random_seed)
    results = {"file":[],"source":[],"target":[],"is_initial":[],"e_dist":[],"lower_bound":[],"upper_bound":[],"approx_length":[],"optimal_length":[],"percent":[],"error":[],"success":[],"o-lb":[]}
    
    nodes = g.nodes.keys()
    corner_nodes = [node for node in nodes if len(g[node].keys()) in [3,4]]
    
    # random_sources = [node for node in random.sample(nodes,num_sources)]
    random_sources = [node for node in random.sample(corner_nodes,num_sources)]
    
    random_edge_dists = list(np.random.rand(num_edge_dist_coefs) / 10)
    for source in random_sources:
        for edge_dist_coef in random_edge_dists:
            # print('SOURCE', source)
            # print('EDGE DIST COEF', edge_dist_coef)
            atomic_result = measure_distance_error_atomic(g,source,edge_dist_coef)
            results["file"].extend([graph_file_path]*len(atomic_result.keys()))
            results["source"].extend(atomic_result["source"])
            results["target"].extend(atomic_result["target"])
            results["is_initial"].extend(atomic_result["is_initial"])
            results["e_dist"].extend(atomic_result["e_dist"])
            results["lower_bound"].extend(atomic_result["lower_bound"])
            results["upper_bound"].extend(atomic_result["upper_bound"])
            results["approx_length"].extend(atomic_result["approx_length"])
            results["optimal_length"].extend(atomic_result["optimal_length"])
            results["percent"].extend(atomic_result["percent"])
            results["error"].extend(atomic_result["error"])
            results["success"].extend(atomic_result["success"])
            results["o-lb"].extend(atomic_result["o-lb"])
            # print("ANOTHER ONE BYTES THE DUST")
    return results


def __time_test__(graph_files,num_sources, num_edge_dist_coefs, random_seed=42, out_file="out.csv"):
    total_results = {"file": [], "node": [], "e_dist": [], "t_scratch": [], "t_prebuilt": [], "t_remains": [], "success": []}
    
    np.random.seed(random_seed)
    random.seed(random_seed)

    # success = True
    failed_infos = []
    fails_count = 0
    total_count = 0
    for graph_file in tqdm(graph_files):
        results = measure_time_graph(graph_file, num_sources, num_edge_dist_coefs)
        for file,node,e_dist,t_scratch,t_prebuilt,t_remains,success in zip(results["file"],results["node"],results["e_dist"],results["t_scratch"],results["t_prebuilt"],results["t_remains"],results["success"]):
            total_count += 1
            if not success:
                fails_count += 1
                failed_infos.append(f"graph_file={graph_file},node={node},e_dist={e_dist},t_scratch={t_scratch},t_prebuilt={t_prebuilt},t_remains={t_remains}")
            total_results["file"].append(file)
            total_results["node"].append(node)
            total_results["e_dist"].append(e_dist)
            total_results["t_scratch"].append(t_scratch)
            total_results["t_prebuilt"].append(t_prebuilt)
            total_results["t_remains"].append(t_remains)
            total_results["success"].append(success)
    df = pd.DataFrame.from_dict(total_results)
    df.to_csv(out_file, sep=",",encoding="utf-8")
    assert fails_count == 0, f"{fails_count}/{total_count}"

def __error_test__(graph_files,num_sources, num_edge_dist_coefs, random_seed=42, out_file="out.csv"):
    total_results = {"file":[],"source":[],"target":[],"is_initial":[],"e_dist":[],"lower_bound":[],"upper_bound":[],"approx_length":[],"optimal_length":[],"percent":[],"error":[],"success":[],"o-lb":[]}
    np.random.seed(random_seed)
    random.seed(random_seed)

    # success = True
    failed_infos = []
    fails_count = 0
    total_count = 0
    for graph_file in tqdm(graph_files):
        results = measure_distance_error_graph(graph_file, num_sources, num_edge_dist_coefs)
        for z in zip(results["file"],results["source"],results["target"],results["is_initial"],results["e_dist"],results["lower_bound"],results["upper_bound"],results["approx_length"],results["optimal_length"],results["percent"],results["error"],results["success"],results["o-lb"]):
            file,source,target,is_initial,e_dist,lower_bound,upper_bound,approx_length,optimal_length,percent,error,success,o_lb = z
            total_count += 1
            if not success:
                fails_count += 1
                failed_infos.append(z)
            total_results["file"].append(file)
            total_results["source"].append(source)
            total_results["target"].append(target)
            total_results["is_initial"].append(is_initial)
            total_results["e_dist"].append(e_dist)
            total_results["lower_bound"].append(lower_bound)
            total_results["upper_bound"].append(upper_bound)
            total_results["approx_length"].append(approx_length)
            total_results["optimal_length"].append(optimal_length)
            total_results["percent"].append(percent)
            total_results["error"].append(error)
            total_results["success"].append(success)
            total_results["o-lb"].append(o_lb)
    df = pd.DataFrame.from_dict(total_results)
    df.to_csv(out_file, sep=",",encoding="utf-8")
    info = '\n'.join([str(t) for t in failed_infos])
    assert fails_count == 0, f"{fails_count}/{total_count}\n{info}"