import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
from pathlib import Path
import os
import generate_correctness_tests
import generate_time_tests



if __name__ == "__main__":
    g = util.load_graph("tests/resource/basic/graph_22.json")
    source = 0
    pre_spt = util.build_spt_with_nx(g,source)
    edge_to_remove = (2,3)
    g.remove_edge(*edge_to_remove)
    pre_spt.remove_edge(*edge_to_remove)
    spt_lib.build_spt(g,source,pre_spt,debug=True)
    
