from iheap import iHeap
import heapq


class BinHeap(iHeap):
    def __init__(self):
        self.data = list()
    
    def insert(self, item):
        heapq.heappush(self.data, item)
    
    def decrease_key(self, old_item, new_item):
        raise NotImplementedError("not implemented")
    
    def min(self):
        if self.data:
            return self.data[0]
        return None
    
    def extract_min(self):
        return heapq.heappop(self.data)

if __name__ == "__main__":
    h = BinHeap()
    h.insert((-10,'a'))
    h.insert((-10, 'b'))
    h.insert((-20, 'c'))
    # h.decrease_key((-10,'a'),(-20,'a'))
    node = h.extract_min()
    print(node)
    node = h.extract_min()
    print(node)
    node = h.extract_min()
    print(node)
    print(h.min())
    a = 1
