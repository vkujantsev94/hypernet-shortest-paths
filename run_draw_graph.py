import networkx as nx
from typing import Optional, Tuple, List, Set, Dict
from networkx.algorithms import distance_measures
import numpy as np
from numpy.random.mtrand import randint
import util
import matplotlib.pyplot as plt
import fibheap as fh
from fib_heap import FibHeap
import json
from tqdm import tqdm
import time
import spt_lib
from pathlib import Path
import os
import generate_correctness_tests
    
def print_graph(graph_file):
    g = util.load_graph(graph_file)
    f = open("debug.info", "w")
    print(g.edges[0,4])
    for node in g.nodes():
        print(f"{node}:", file=f)
        for n in g[node]:
            print(f"{(node, n)} {g.edges[node,n]['length']}", file=f)

def draw_graph(graph_file):
    g = util.load_graph(graph_file)
    s = list(g.nodes)[0]
    print("dr0")
    nx.draw_networkx(g, pos=util.get_pos(g))
    print("dr1")
    labels = nx.get_edge_attributes(g,'length')
    labels = {k: "{:.2f}".format(v) for k,v in labels.items()}
    nx.draw_networkx_edge_labels(g, pos=util.get_pos(g), edge_labels=labels)
    print("dr2")
    plt.show()
    
if __name__ == "__main__":
    #['resource\\1000\\graph1000-0'], source_distance_coefs = [0, 1], num_sources = 2, edge_portion = 0.1
    # print_graph('tests/resource\\10\\graph10-293')
    draw_graph('tests/resource/hex/100/hex100-0.json')
