import numpy as np
from graph import *  # Graph
from fuzzy import FuzzyNumber
from edge import Edge
from predictor import MaxFlowPredictor, rollback
import json


def on_one_line():
    pass


def check_reverse_edge(ind_pair, connections):
    i, j = ind_pair
    for e in connections:
        if e == (i, j) or e == (j, i):
            return True
    return False


def generate_graph(width, height, non_zero=False, fuzzificate=False, random_state=42, dump_path="",
                   verbose=False) -> Graph:
    """
    generates net-like graph with non-fuzzy flows and capacities
        flows satisfy flow conservation
    :param width: width of middle-nodes part
    :param height: height of middle-nodes part
    the graph below has width=2, height=3:
        *   *
    *   *   *   *
        *   *
    :param random_state:
    :return: graph object
    """
    np.random.seed(random_state)
    num_middle_verts = width * height

    connections = set()
    unravel_index = lambda ind: (ind % height, ind // height)
    ravel_index = lambda ind_ij: ind_ij[1] * height + ind_ij[0]
    sort_pair = lambda i, j: (min(i, j), max(i, j))
    get_prob = lambda threshold: np.random.random() < threshold
    #  nodes on each which have incoming edge. must have outcome
    nodes_with_income = [set() for i in range(width)]

    mapping_func = lambda rav_ind: unravel_index(rav_ind)[0]
    for ind_to in range(width):
        row_inds = np.arange(height) if ind_to == 0 else np.array(list(map(mapping_func, nodes_with_income[ind_to])))
        np.random.shuffle(row_inds)
        for ind_from in row_inds:
            ravel_cur_ind = ravel_index((ind_from, ind_to))
            prob_threshold = 0.75 if ind_to == 0 else 0.5  # magic constant
            add_upper_connection = ind_from - 1 >= 0 and get_prob(prob_threshold)
            add_lower_connection = ind_from + 1 < height and get_prob(prob_threshold)
            if add_upper_connection:
                ravel_ind_to = ravel_index((ind_from - 1, ind_to))
                nodes_with_income[ind_to].add(ravel_ind_to)
                # ind_pair = sort_pair(ravel_cur_ind, ravel_ind_to)
                ind_pair = (ravel_cur_ind, ravel_ind_to)
                if ind_pair not in connections and not check_reverse_edge(ind_pair, connections):
                    connections.add(ind_pair)
            if add_lower_connection:
                ravel_ind_to = ravel_index((ind_from + 1, ind_to))
                nodes_with_income[ind_to].add(ravel_ind_to)
                ind_pair = (ravel_cur_ind, ravel_ind_to)
                if ind_pair not in connections and not check_reverse_edge(ind_pair, connections):
                    connections.add(ind_pair)
        if ind_to == width - 1:
            continue
        for ravel_node_ind in nodes_with_income[ind_to]:
            ravel_inds_to = np.arange((ind_to + 1) * height, num_middle_verts)
            np.random.shuffle(ravel_inds_to)
            num_edges_created = 0
            while num_edges_created <= np.random.randint(height // 1):  # magic constant
                rand_ravel_ind = ravel_inds_to[num_edges_created]
                ind_pair = (ravel_node_ind, rand_ravel_ind)
                if ind_pair in connections:
                    continue
                rand_i, rand_j = unravel_index(rand_ravel_ind)
                nodes_with_income[rand_j].add(rand_ravel_ind)
                connections.add(ind_pair)
                num_edges_created += 1

    #  create graph object manually, add source, add sink
    #  flow is set to 0
    #  capacity for middle nodes is chosen randomly
    #  capacity for source and sink is chosen as very big value so the flows wont block
    g = Graph()
    g.source = 0
    g.sink = num_middle_verts + 1
    g.num_vertices = num_middle_verts + 2
    g.graph = [[] for i in range(g.num_vertices)]

    for ind_pair in connections:
        ind_from, ind_to = ind_pair
        ind_from, ind_to = ind_from + 1, ind_to + 1
        g.add_edge(Edge(ind_from, ind_to, cap, flow, 1))

    #  add source and edges to first-layer-nodes if they have incomes or outcomes
    first_layer_inds = set(np.array(list(nodes_with_income[0])) + 1)
    first_layer_inds |= {i for i in range(height) if len(g[i]) > 0}
    first_layer_inds = np.array(list(first_layer_inds))
    for ind_to in first_layer_inds:
        ind_from = 0
        g.add_edge(Edge(ind_from, ind_to, cap, flow, 1))

    #  add sink and connect last-layer-nodes if they have incomes
    last_layer_inds = np.array(list(nodes_with_income[width - 1])) + 1
    for ind_from in last_layer_inds:
        ind_to = g.sink
        g.add_edge(Edge(ind_from, ind_to, cap, flow, 1))
    if verbose:
        print(g.pretty())

    if verbose:
        print('\t\tfinal')
        print(final_graph.pretty())

    #  dump to file
    if dump_path:
        g_json = aug_gr.to_json()
        with open(dump_path + f"gen_graph_clear{random_state}.json", 'w') as outfile:
            json.dump(g_json, outfile)
    return final_graph


def check_flow_conservation(g):
    inflows = [FuzzyNumber(0) for i in range(g.num_vertices)]
    outflows = [FuzzyNumber(0) for i in range(g.num_vertices)]
    for e in g.get_edge_iterator(with_reversed=False):
        node_from = e.vert_from
        node_to = e.vert_to
        inflows[node_to] += e.flow
        outflows[node_from] += e.flow
    check = True
    for i in range(1, g.num_vertices - 1):
        node_check = inflows[i] == outflows[i]
        # if not node_check:
            # print(f"flows are incorrect at node {i}")
        check = check and node_check
    return check


def fuzzificate_graph(g: Graph, random_state=42, verbose=False) -> Graph:
    #  asssert, что граф - четкий
    assert not g.is_fuzzy(), "Provided graph is already fuzzy"
    g = g.__copy__()
    #  взять рандомно ребра (например Х% от тех, у которых не нулевые потоки)
    np.random.seed(random_state)

    def cond(e):
        cond1 = e.flow > FuzzyNumber(0)
        cond2 = e.vert_from != g.source
        cond3 = e.vert_to != g.sink
        return cond1 and cond2 and cond3

    edges_to_fuzz = np.array([e for e in g.get_edge_iterator() if cond(e)])
    np.random.shuffle(edges_to_fuzz)
    num_fuzz_ratio = len(edges_to_fuzz) // 2
    edges_to_fuzz = edges_to_fuzz[0: num_fuzz_ratio]

    #  определить, насколько их можно размыть,
    #  добавить рандомное размытие в пределах (пусть целочисленно),
    #  пометить, чтобы в предикте не занулились
    for e in edges_to_fuzz:
        valid_fuzz_limit = (e.capacity - e.flow).val
        fuzz_val = np.random.randint(1, min(valid_fuzz_limit, 3))
        e.flow.l = fuzz_val
        e.flow.r = fuzz_val
        e.mark = True
    #  сделать predict
    predictor = MaxFlowPredictor(g)
    predicted = predictor.predict(with_reversed=False)

    return predicted


def remove_zero_edges(g: Graph):
    for e in g.get_edge_iterator(with_reversed=False):
        if e.flow == FuzzyNumber(0):
            g.remove_edge((e.vert_from, e.vert_to))


# generated_graph = generate_graph(4, 3, random_state=42, dump_path='./gen_graph.json', verbose=True)
generated_graph = generate_graph(3, 3, random_state=42, fuzzificate=False, verbose=False)
remove_zero_edges(generated_graph)
with open(f'./generated_demo/graph42.json', 'w') as f:
    json.dump(generated_graph.to_json(), f)

with open('generated_demo/demo_before.dot', 'w') as f:
    f.write(generated_graph.to_dot())

# n = 1000
# f_count = 0
# for i in range(n):
#     try:
#         generated_graph = generate_graph(3, 3, random_state=i, fuzzificate=True,
#                                          dump_path=f'./generated_exp2/', verbose=False)
#         # print('*' * 40)
#         # print(generated_graph.pretty())
#         # test reading from json
#         # read_graph = Graph('./gen_graph.json')
#         # print(read_graph.pretty())
#         # fuzzificated_graph = fuzzificate_graph(read_graph, random_state=i, verbose=False)
#         # print(fuzzificated_graph.pretty())
#         # print(check_flow_conservation(read_graph))
#     except Exception as e:
#         f_count += 1
#         print(e)
# print(f"{f_count} fails, {n - f_count} succeeded")
